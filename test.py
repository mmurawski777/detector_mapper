
from src.daq.usb4716 import Usb4716
import matplotlib.pylab as plt
import ctypes
from procedury import *
import os
import time
import threading

#parametry
M=10
dx = 0.01
L=2 #mm


motors=[]
fc=99.0
fp=10e3
v = fc*dx/M
N = int(M*fp/fc)


buffor_len=int(L*M*fp/fc/dx)
print(buffor_len)
K=int(buffor_len/N)
_dir = os.getcwd()
_dll = _dir+"\\daq.dll"
_xml = _dir+"\\profile.xml"
x0=12.29
y0=9.51
start = x0-L/2
print("start ",start)
@ctypes.CFUNCTYPE(None)
def done():
    print("its done")

daq = Usb4716(device_description="USB-4716,BID#1",profile_path=_xml,
start_channel=14,channel_count=2,section_length=buffor_len,section_count=0,call_back=done)



if __name__=="__main__":
    try:
        from src import apt
        x = apt.list_available_devices()
        if(x.__len__()<2):
            raise Exception("Nie znaleziono dwoch manipulatorow",x)
            del apt
        print("znaleziono 2 manipulatory",x)
    except Exception as e:
        print(e)
        apt._apt._cleanup()
        del apt
    motors.append(apt.Motor(x[0][1],0))
    motors.append(apt.Motor(x[1][1],1))
    print("kanal aktywny manipulatora x",motors[0].active_channel)
    print("kanal aktywny manipulatora y",motors[1].active_channel)
    print("x: ",motors[0].position)
    print("y: ",motors[1].position)
    motors[0].move_to(x0,True)
    motors[1].move_to(y0,True)
    #daq.getData()
    #plt.plot(daq.get_sig()[0:N])
    print(motors[1].get_velocity_parameters())
    print("liczba okresow ",M)
    print("liczba probek na dx ", N)
    print("dx ",dx)
    print("fc ", fc)
    print("fp ",fp)
    print("v ",v," mm/s ")
    print("L ",L," mm ")
    print("buffor len ",buffor_len)
    print("K ",K)
    motors[0].set_velocity_parameters(
        0,
        2.9,
        1.5    
    )
    print(motors[0].get_velocity_parameters())
    motors[0].move_to(start-1,True)
    motors[0].set_velocity_parameters(
        0,
        2.9,
        v
    )
    thread = threading.Thread(target=motors[0].move_to,args=(start+L+1,True))
    thread2 = threading.Thread(target=daq.getData,args=())
    thread.start()
    while(motors[0].position<start):
        pass
        #print(motors[0].position)
    thread2.start()
    thread2.join()
    plt.plot(daq.get_sig())
    print(motors[0].position)
    print(motors[1].position)
    plt.show()
    arr = []
    for i in range(0,K):
        sig = daq.get_sig()[i*N:(i+1)*N]
        ref = daq.get_ref()[i*N:(i+1)*N]

        idx1=[i for i,x in enumerate(np.diff(ref)) if x>1]
        idx2=[y for x,y in zip(np.diff(idx1),idx1[:-1]) if x>3]

        if(idx1[-1]-idx2[-1]>3):
            idx = np.append(idx2,idx1[-1])
        else:
            idx = idx1

        sig = sig[idx[0]:idx[-1]]
        ref = ref[idx[0]:idx[-1]]
        ref_dc = np.average(ref)
        ref = (ref -ref_dc)/5.0

        shift = int((idx[1]-idx[0])/4)

        y=2*np.average(sig*ref)
        x=2*np.average(sig*np.append(ref[shift:],ref[shift-1::-1]))
        ac=np.sqrt(x*x+y*y)
        arr.append(ac)

    plt.plot(np.arange(0,arr.__len__())*dx,np.asarray(arr))
    plt.show()
    
    
    
    


