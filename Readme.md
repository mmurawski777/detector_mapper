## Instalacja
Do przeprowadzenia instalacji potrzebne są następujące pliki i programy
* APT.dll - w folderze src/apt/
* Python w wersji 32-bitowej
* Biblioteka matplotlib
* Kompilator g++. Najlepiej MinGW
* Oprogramowanie Navigator firmy Advantech ze sterownikiem do karty akwizycji danych

* Przeporwadzneie instalacji
    * Po zainstalowaniu Pythona kompilację potrzebnych plików można wykonanać uruchamiając plik setup.bat. W razie nie powodzenia należy wykonać poniższe komendy manualnie w konsoli
    * kompilacja Pliku Usb4716.dll - odpowiedzialnego za komunickację z kartą akwizycyjną 
        
            >> g++ -c -o src/daq/Usb4716.o src/daq/Usb4716.cpp src/daq/Usb4716.cpp, 
            >> g++ -shared -o src/daq/Usb4716.dll src/daq/Usb4716.o
    * instalacja biblioteki matplotlib 
        
            >> python -mpip install matplotlib
            >> python -mpip install numpy
    * Jeśli w folderze src/db nie znajduje się plik test.db należy go utworzyć wywołując fukcje init_db(name)

            >> python
            >> from src.db.sqlite_functions
            >> init_db()



## Instrukcja Uruchomienia
* Uruchom plik setup.bat
* Umieść plik APT.dll z poprzedniej wersji do folderu /src/apt
* Wpisz parametry pomiaru
    ** filename - nazwa pliku
    ** x_range,y_range -zakresy osi
    ** x_div,y_div - podziałka osi
* Ustaw czas akwizycji/ilość próbek
* przesuń suwak okleślający czas akwizycji lub wpisz ilośc próbek i wcisnij SET
* Uruchom karte akwizycji przyciskiem OSC:ON
* Włącz manipulatory Thorlabsa przyciskiem INICJALIZACJA APT
    **  WYŚRODKUJ - przesuwa ławę do zadeklaownej pozycji zera. Domyślnie (0,0)
    ** ZNAJDZ ŚRODEK - program znajduje środek na podstawie maksymalnych zakresów
    ** USTAW ŚRODEK - program przyjmuje obecny punkt jako nowy środek
    ** <<,>> - przystrzymanie tych przycisków spowoduje poruszanie się uchwytu w danym kierunku
* Po skalibrowaniu pozycji uchwytu uruchom pomiar przyciskiem START
* Opcje zapisu do różnych formatów znajdują się w górnym menu pod przyskiem Plik
* UWAGA: dla bezpieczeństwa działania programu zalecam nie zmieniać parametrów w trakcie pomiaru tj. - zmieniać stałą czasową

## Opis folderów

dettector_mapper  
│   README.md  
│   program.py -- uruchamia program z pythona  
|   setup.bat -- kompuluje program z konsoli  
|   setup.py -- kompuluje program z pythona  
|   uruchom.bat -- uruchomienie programu z konsoli  
|  
│  
└───src  
   │   __init__.py  
   └───apt - bibliotek apt  
   │   │   ...  
   └───daq - bibliotek obsługi karty pomiarowej  
   │   │   ...  
   └───db - baza danych  
   │   │   ...  
   └───components - Elementu UI  
   │    │   ...  
   │images - folder ze zdjęciami pomiaru  
   │   │   ...  
   └───tests - folder z testami  
        │   ...  
