"""@package docstring
Dokumentacja modułu program
Jest to moduł który przechwouje głowny interfejs użytkownika
w jego wnętrzu są zadeklarowane komponenty z modułu components.
Przechwouje defincje funkcji które są podłączone do funckji obiektów interfejsu.
Bardziej skomplikowane funkcje np procedura pomiarowa są przechowywane przez moduł procedury.py
"""

from src.components.panels.settings_panel import SettingsPanel
from src.components.panels.dmap import DMap
from src.components.panels.menu_bar import MenuBar
from src.components.panels.signals_plot import signals_plot_init
from src.daq.daq_panel import DaqPanel
import matplotlib.pyplot as plt
from datetime import datetime
from tkinter import messagebox,ttk
from src.daq.usb4716 import Usb4716
from procedury import *
from tkinter import *
from tkinter import filedialog
from numpy.fft import rfft
import time,threading,os
import multiprocessing
import matplotlib.animation as animation
import numpy as np
import re
from src.utils import *
import os
import ctypes
import re

@ctypes.CFUNCTYPE(None)
def done():
    pass
#inicjalizajca zmiennych globalnych
root=Tk()   #główny obiekt biblioteki realizującej UI
root.title("detector mapper")
motors=[]   #tablica przechowująca obiekty do manipulatorów APT
daq= Usb4716(section_length=1024,call_back=done)  #obiekt obslugujący kartę pomiarową
threads=[]  #tablica wątków
grid = np.zeros(shape=(10,10))  #tablica startowa
#deklaracje eventów
pasue = threading.Event()   
apt_init_event = threading.Event()
daq_init_event = threading.Event()
osc_init_event = threading.Event()
find_middle_event = threading.Event()
go_to_middle_event = threading.Event()
start_event = threading.Event()
move_motor_event = threading.Event()
running = threading.Event()
running.set()
#pasue.set()
settings_cfg=load_obj(os.getcwd()+"//src//settings.cfg")
time_axis = np.arange(0,1024)*0.05


def switch_device(event):
    """
        funkcja do przełączania urządzeń 
    """
    global daq,done
    #osc_btn_fun()
    if(osc_init_event.is_set()):
        osc_btn_fun()
    
    time.sleep(0.5)
    if(daq_panel.option_mnu_var.get()[0:2]=="DQ"):
        daq.reinit(section_length=1024,call_back=done)
        #change_section_length()
    elif(daq_panel.option_mnu_var.get()[0:2]=="NV"):
        daq.nanovolotmeter(call_back=done)
        change_section_length()
    else:
        print("pass")
        pass

def acquire_data(event,daq):
    """
        funkcja do ciągłej akwizycji dannych przez kartę pomiarową
    """
    while(event.is_set()):
        time.sleep(0.1)
        daq.getData()

def osc_fun(event,daq_panel,daq):
    """
        funkcja do ciągłego wyświetlania pomiarów
        Args:
            event -
            daq_panel - panel przechowujący informacje o sygnale. Realizuje to ciągłe śledzenie fazy i amplitudy
    """
    while(event.is_set()):
        try:
            if(daq.device_type=="NV"):
                x=daq.getBuffer()
                #line1.set_ydata(x[::2]) #ref
                line2.set_ydata(x[1::2]) #sygnal
                daq_panel.set_parameters(round(np.average(x[1::2]),4),0,0,0)
                signal_canvas.draw()
            else:
                sig=daq.get_sig()
                ref=daq.get_ref()
                
                try:
                    idx=[i for i,x in enumerate(np.diff(ref)) if x>1] # znajdowanie zboczy narastających
                    idx=[y for x,y in zip(np.diff(idx),idx[:-1]) if x>3] # wycinanie podwójnych punktów na zboczach liczba 3 z tw. Nyquista
                    ref2=ref[idx[0]:idx[0]+3*int(daq_panel.section_length/4)]
                    sig2=sig[idx[0]:idx[0]+3*int(daq_panel.section_length/4)]
                    line1.set_data(time_axis[:ref2.__len__()],ref2)
                    line2.set_data(time_axis[:sig2.__len__()],sig2)
                    ref=ref[idx[0]:idx[-1]]
                    a,b,c,d=lockin(signal=sig[idx[0]:idx[-1]],trigger=ref,period=idx[1]-idx[0],time_const=0.000025)
                    daq_panel.set_parameters(a,b,c,d)
                except Exception as e2:
                    print(e2)
                    print("OSC FUN ERROR")
                    line1.set_data(time_axis,daq.get_ref())
                    line2.set_data(time_axis,daq.get_sig())
                signal_canvas.draw()
        except Exception as e:
            print("len - ",daq.getBuffer().__len__())
            line1.set_data(time_axis,daq.get_ref())
            line2.set_data(time_axis,daq.get_sig())
            signal_canvas.draw()
            print("nie udało się obliczyć parametrow sygnalu")


def change_section_length(additional_samples=0):
    """
        Funkcja do zmiany długości sekcji pobieranej przez karte akwizycji dnaych
    """
    #zatrzymanie pracującego wątku
    if(osc_init_event.is_set()):osc_btn_fun()
    global fig,ax,line1,line2,signal_canvas,daq,done
    line1.set_data(np.arange(0,daq_panel.section_length)*0.05, np.ones(daq_panel.section_length))
    line2.set_data(np.arange(0,daq_panel.section_length)*0.05, np.ones(daq_panel.section_length))
    ax[0].relim()
    ax[0].autoscale_view(True,True,True)
    ax[1].relim()
    ax[1].autoscale_view(True,True,True)     
    signal_canvas.draw()
    if(daq.device_type=="NV"):
        daq.nanovolotmeter(section_length=daq_panel.section_length,call_back=done)
    else:
        daq = Usb4716(section_length=daq_panel.section_length,call_back=done)

#funkcja do włączania i wyłączania przebiegu widzianego z karty pomiarowej
def osc_btn_fun():
    """
        Funkcja uruchamia wątki do ciągłej akwizycji danych i równoległego ich wyświetlania. Funkcja uruchamia zdarzenia
        i wątki dla funkcji osc_fun acquire_data
    """
    global osc_init_event,panel,threads,daq
    if(osc_init_event.is_set()):
        osc_init_event.clear()
        panel.osc_var.set("OSC:  ON")
    else:
        panel.osc_var.set("OSC: OFF")
        osc_init_event.set()
        threads.append(threading.Thread(target=acquire_data,args=(osc_init_event,daq)))
        threads.append(threading.Thread(target=osc_fun,args=(osc_init_event,daq_panel,daq)))
        threads[-1].start()
        threads[-2].start()

def apt_init_btn_fun():

    """
        funkcja inicjalizacji protokołu APT
    """
    if not apt_init_event.is_set():
        global find_middle_event,panel,running
        threads.append(threading.Thread(target=initialize_motors,args=(apt_init_event,panel,
        motors,running)))
        threads[-1].start()
        apt_init_event.set()

def find_middle_btn_fun():
    """
        Funkcja do przycisku znajdź środek, obsługuje wyjątek, łączy się z UI, uruchamia procedurę odnajdowania środka
    """
    try:
        global find_middle_event,panel,motors
        if(find_middle_event.is_set()):
            find_middle_event.clear()
        else:
            threads.append(threading.Thread(target=find_middle,args=(find_middle_event,motors,panel)))
            threads[-1].start()
            find_middle_event.set()
    except Exception as e:
        print(e)
        #error_canvas.create_oval(5, 5, 35, 35, outline="#f11",fill="#f11", width=2)
        #error_box.insert(INSERT,datetime.now().strftime('%Y-%m-%d:%H:%M:%S')+" "+repr(e)+"\n")


def go_to_middle_fun():
    """
        Funkcja do przycisku wyśrodkuj, obsługuje wyjątek i łączy się z intefejsem.
        Implementuje procedurę wyśrodkowania
    """
    try:
        global go_to_middle_event,panel,motors
        if(go_to_middle_event.is_set()):
            go_to_middle_event.clear()
        else:
            threads.append(threading.Thread(target=go_to_middle,args=(go_to_middle_event,motors,panel)))
            threads[-1].start()
            go_to_middle_event.set()
    except Exception as e:
        print(e)
        #error_canvas.create_oval(5, 5, 35, 35, outline="#f11",fill="#f11", width=2)
        #error_box.insert(INSERT,datetime.now().strftime('%Y-%m-%d:%H:%M:%S')+" "+repr(e)+"\n")

def set_middle_fun():
    """
        Funkcja ustawiająca obecną pozycję manipulatora jako nowy środek
    """
    try:
        panel.x0=motors[0].position
        panel.y0=motors[1].position
        panel.x=motors[0].position
        panel.y=motors[1].position
    except Exception as e:
        print(e)

def move_x_forward(event):
    """
        Funkcja interfejsu do przycisku poruszająca silnikiem
    """
    try:
        move_motor_event.set()
        threading.Thread(target=move_motor,args=(move_motor_event,motors[0],0.1)).start()
    except Exception as e:
        print(e)
        #error_canvas.create_oval(5, 5, 35, 35, outline="#f11",fill="#f11", width=2)
        #error_box.insert(INSERT,datetime.now().strftime('%Y-%m-%d:%H:%M:%S')+" "+repr(e)+"\n")

def move_x_backward(event):
    try:
        move_motor_event.set()
        threading.Thread(target=move_motor,args=(move_motor_event,motors[0],-0.1)).start()
    except Exception as e:
        print(e)
        #error_canvas.create_oval(5, 5, 35, 35, outline="#f11",fill="#f11", width=2)
        #error_box.insert(INSERT,datetime.now().strftime('%Y-%m-%d:%H:%M:%S')+" "+repr(e)+"\n")

def move_y_forward(event):
    try:
        move_motor_event.set()
        threading.Thread(target=move_motor,args=(move_motor_event,motors[1],0.1)).start()
    except Exception as e:
        print(e)
        #error_canvas.create_oval(5, 5, 35, 35, outline="#f11",fill="#f11", width=2)
        #error_box.insert(INSERT,datetime.now().strftime('%Y-%m-%d:%H:%M:%S')+" "+repr(e)+"\n")

def move_y_backward(event):
    try:
       move_motor_event.set()
       threading.Thread(target=move_motor,args=(move_motor_event,motors[1],-0.1)).start()
    except Exception as e:
        print(e)
        #error_canvas.create_oval(5, 5, 35, 35, outline="#f11",fill="#f11", width=2)
        #error_box.insert(INSERT,datetime.now().strftime('%Y-%m-%d:%H:%M:%S')+" "+repr(e)+"\n")

def stop_motor(event):
    global move_motor_event
    move_motor_event.clear()



"""
    Uruchamiania i zatrzymywanie pomiaru
"""
def start_btn_fun():
    global daq, done
    """
        funkcja do uruchamiania procedury pomiarowej
    """
    try:
        if(osc_init_event.is_set()):
            pass
        if(start_event.is_set()):
            start_event.clear()
            panel.start_btn['text']='START'
            panel.stop_btn['state']='disabled'
        else:
            panel.start_btn['state']='disabled'
            panel.start_btn['text']='STOP'
            panel.stop_btn['state']='active'
            panel.stop_btn['text']='PAUSE'
            if(daq_panel.option_mnu_var.get()=="DQ 1D measurement"):
                threads.append(threading.Thread(target=line_measurement,args=
                (start_event,pasue,go_to_middle_event,motors,panel,amp_maps[0],ph_maps[0],amp_cos,
                daq_panel,menubar.go_to_max.get())))
                threads[-1].start()
                start_event.set()
            elif(daq_panel.option_mnu_var.get()=="NV"):
                if(daq.device_type=="NV"):
                    threads.append(threading.Thread(target=sequence_meassurement_nanovoltometer,
                    args=(start_event,pasue,go_to_middle_event,motors,panel,amp_maps[0],ph_maps[0],daq_panel)))
                    threads[-1].start()
                    start_event.set()
            elif(daq_panel.option_mnu_var.get()=="DQ fast measurement"):
                if(osc_init_event.is_set()):osc_btn_fun()
                threads.append(threading.Thread(target=fast_meassurement,
                args=(start_event,pasue,go_to_middle_event,motors,panel,amp_maps[0],ph_maps[0],amp_cos,daq_panel,
                menubar.go_to_max.get(),daq,done)))
                threads[-1].start()
                start_event.set()
            elif(daq_panel.option_mnu_var.get()=="DQ slow measurement"):
                if(not osc_init_event.is_set()):osc_btn_fun()
                threads.append(threading.Thread(target=sequence_meassurement,
                args=(start_event,pasue,go_to_middle_event,motors,panel,amp_maps[0],ph_maps[0],amp_cos,daq_panel,
                menubar.go_to_max.get())))
                threads[-1].start()
                start_event.set()
            elif(daq_panel.option_mnu_var.get()=="DQ fast measurement 4CH"):
                if(osc_init_event.is_set()):osc_btn_fun()
                threads.append(threading.Thread(target=fast_meassurement_4ch,
                args=(start_event,pasue,go_to_middle_event,motors,panel,amp_maps,ph_maps,daq_panel,
                menubar.go_to_max.get(),daq,done)))
                threads[-1].start()
                start_event.set()
            else:
                print("pass")

    except Exception as e:
        pass
            #error_canvas.create_oval(5, 5, 35, 35, outline="#f11",fill="#f11", width=2)
            #error_box.insert(INSERT,datetime.now().strftime('%Y-%m-%d:%H:%M:%S')+" "+repr(e)+"\n")

def pasue_btn_fun():
    """
        funkcja do zatrzymywania pomiaru, ustawia lub czyści eventy
    """
    global pasue,panel
    if(not pasue.is_set()):
        pasue.set()
        panel.stop_btn['text']='RESUME'
        panel.start_btn['state']='active'
    else:
        panel.stop_btn['text']='PAUSE'
        panel.start_btn['state']='disabled'
        pasue.clear()


def close():
    """
        Funkcja realizująca procedurę zamykanie tzn. zamykamy pracujące wątki
    """
    global osc_init_event,running
    osc_init_event.clear()
    running.clear()
    time.sleep(2)
    root.quit()
    root.destroy()
    
def on_closing():
    """
        Komunikat w trakcie zamknięcia
    """
    if messagebox.askokcancel("Quit", "Do you want to quit?"):
        close()


"""
    Zapisywanie wyników funkcje przypisane do obiektu menubar
"""
def file_save_as_csv_amp():
    filename =  filedialog.asksaveasfilename(initialdir = settings_cfg["last_dir"],title = "Wybierz plik",filetypes = (("csv","*.csv"),("wszystkie pliki","*")))
    np.savetxt(filename+".csv", np.flip(amp_maps[0].get_array(),0), delimiter=",",newline='\n')
    settings_cfg["last_dir"]=re.sub("\/[^\/]+$","",filename)
    save_obj(settings_cfg,os.getcwd()+"//src//settings.cfg")

def file_save_as_png_amp():
    filename =  filedialog.asksaveasfilename(initialdir = settings_cfg["last_dir"],title = "Wybierz plik",filetypes = (("png","*.png"),("wszystkie pliki","*")))
    amp_maps[0].fig.savefig(filename)
    settings_cfg["last_dir"]=re.sub("\/[^\/]+$","",filename)
    save_obj(settings_cfg,os.getcwd()+"//src//settings.cfg")

def file_save_as_csv_phase():
    filename =  filedialog.asksaveasfilename(initialdir = settings_cfg["last_dir"],title = "Wybierz plik",filetypes = (("csv","*.csv"),("wszystkie pliki","*")))
    np.savetxt(filename+".csv",np.flip(ph_maps[0].get_array(),0), delimiter=",",newline='\n')
    settings_cfg["last_dir"]=re.sub("\/[^\/]+$","",filename)
    save_obj(settings_cfg,os.getcwd()+"//src//settings.cfg")

def file_save_as_png_phase():
    filename =  filedialog.asksaveasfilename(initialdir = settings_cfg["last_dir"],title = "Wybierz plik",filetypes = (("png","*.png"),("wszystkie pliki","*")))
    ph_maps[0].fig.savefig(filename)
    settings_cfg["last_dir"]=re.sub("\/[^\/]+$","",filename)
    save_obj(settings_cfg,os.getcwd()+"//src//settings.cfg")

def hello():
    pass


"""
    Rysowanie interfejsu
    Składanie tworzenie panelów i podłączanie do niego powyższych funkcji
"""

def move_to_x():
    motors[0].move_to(panel.x)
def move_to_y():
    motors[1].move_to(panel.y)
def open_from_db():
    global panel,amp_maps,ph_maps,root
    top_lvl = Toplevel()
    box = Listbox(top_lvl,width=150)
    box.grid(column=0,row=1)
    search_var = StringVar()
    search_entry = Entry(top_lvl,textvariable=search_var,width=25)
    search_entry.grid(column=0,row=0,sticky="NW")
    def search(*args):
        data = select_map_info_filename(file_name=search_var.get())
        box.delete(0, END)
        for i in range(0,data.__len__()):
            box.insert(i,str(data[i]))
    def fun(*args):
        var = box.get(box.curselection())
        m = re.search('(\d+)',var)
        print(amp_maps.__len__())
        print(int(m.group(0)))
        display_data(int(m.group(0)),panel,amp_maps[0],ph_maps[0])
    box.bind('<Double-1>', fun)
    btn = Button(top_lvl, text="search", command=search)
    btn.grid(column=0,row=0,columnspan=2)
    search_entry.bind("<Return>", search)
#Rysowanie górnego panelu menu do zapisywanie plików
menubar=MenuBar(root,file_save_as_csv_amp=file_save_as_csv_amp,file_save_as_png_amp=file_save_as_png_amp,
            file_save_as_csv_phase=file_save_as_csv_phase,file_save_as_png_phase=file_save_as_png_phase,open_from_db=open_from_db,hello=None)
panel = SettingsPanel(root,0,0,osc_btn_fun,apt_init_btn_fun,find_middle_btn_fun,
                    go_to_middle_fun,move_x_forward,move_x_backward,move_y_forward,move_y_backward,set_middle_fun,
                    start_btn_fun,pasue_btn_fun,stop_motor,move_to_x,move_to_y)
                    
#zakładki na wykresy
tabs = ttk.Notebook(root)
amp_tabs = []
amp_maps =[]
ph_maps = []
ph_tabs = []
for i in range(0,4):
    amp_tabs.append(ttk.Frame(tabs))
    ph_tabs.append(ttk.Frame(tabs))
    tabs.add(amp_tabs[-1], text='Amp J'+str(i+1))
    tabs.add(ph_tabs[-1], text='Phase J'+str(i+1))
      

tab_signal = ttk.Frame(tabs)  
tabs.add(tab_signal, text='Signal')
tab_amp_cos = ttk.Frame(tabs)  
tabs.add(tab_amp_cos, text='Amp*Cos')
tabs.grid(column=6,row=0,rowspan=30)
daq_panel = DaqPanel(root,0,16,re_init_daq=change_section_length,switch=switch_device)



for i in range(0,4):
    amp_maps.append(
        DMap(amp_tabs[i],0,0,1,scale="log",vmin=0.001,vmax=15.0)
    )
    ph_maps.append(
        DMap(ph_tabs[i],0,0,1,scale="lin",vmin=-180,vmax=180)
    )

amp_cos=DMap(tab_amp_cos,0,0,1,scale="lin",vmin=-5,vmax=5)
#error_canvas = Canvas(root,  width=45, height=45)
#error_canvas.grid(column=0,row=31,sticky="NW")
#error_canvas.create_oval(5, 5, 35, 35, outline="#f11",fill="#1f1", width=2)

#error_box = Text(root,height=5)
#error_box.bind("<Key>", lambda e: "break")
#error_box.bind("<1>", lambda event: error_box.focus_set())
#error_box.grid(column=6,row=31,sticky="WE")

root.protocol("WM_DELETE_WINDOW", on_closing)
#sygnaly  
fig,ax,line1,line2,signal_canvas = signals_plot_init(tab_signal,daq_panel.section_length,0.05)
root.mainloop()
