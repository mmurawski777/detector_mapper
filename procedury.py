"""
    Moduł przechowujący procedury pomiarowe i kalibracyjne
"""

import numpy as np
import threading

import matplotlib.pyplot as plt
from numpy.fft import fft
from tkinter import messagebox
from src.db.sqlite_functions import *
import time
_dir = os.getcwd()
_db = _dir+"\\src\\db\\test.db"

def motor_parallel_move(motors,x,y):
    """
        Funkcja do równoległego poruszania się manipulatorów
        Args:
            motors - tablica obiektów Motors z APT
            x - pozycja w osi x
            y - pozycja w osi y
    """
    threads=[]
    threads.append(threading.Thread(target=motors[1].move_to,args=(y,True,)))
    threads.append(threading.Thread(target=motors[0].move_to,args=(x,True,)))
    for i in threads: i.start()
    for i in threads: i.join()

def exit_sequence(panel,daq_panel,event_stop,event_start):
    """
        Funkcja sekwencji wyjścia
        Args:
            panel - panel przechowujący przyciski start,stop
            event_stop - zdarzenie stopu
            event - zdarzenie startu
    """
    panel.enable()
    daq_panel.enable()
    panel.start_btn['text']='START'
    panel.start_btn['state']='active'
    panel.stop_btn['text']='PAUSE'
    panel.stop_btn['state']='disable'
    event_stop.clear()
    event_start.clear()

def monitor_position(motors,panel,event):
    """
        Funkcja do ciągłego śledzenia pozycji silników APT
        Args:
            motors - tablica obiektór Motorts z APT
            panel - panel przypisujący pozycje
            event - zdarzenie
    """
    while(event.is_set()):
        if panel.x_entry['state']=='disabled':
            panel.x=motors[0].position
        if panel.y_entry['state']=='disabled':
            panel.y=motors[1].position

def move_motor(event,motor,inc):
    """
        Funkcja do ciągłego prouszania silnikiem
        Args:
            event - zdarzenie synchronizujące
            motor - obiekt silnika apt
            inc - krok zadany w [mm]
    """
    while(event.is_set()):
        motor.move_by(inc,True)

def find_middle_of_one(motor):
    """
        Procedura znajdująca środek dla jednego silnika
        Args:
            motor - obiekt silnika apt
    """
    motor.move_home(True)
    motor.move_to(25,True)
    middle=motor.position/2.0
    motor.move_to(middle,True)


def go_to_middle(event,motors,panel):
    """
        Procedura znajdująca środek
        Args:
            event - event uruchami procedure kiedy zostanie ustawiony
            motors - tablica silników thorlabs
            panel - obiekt UI przechowujący x0,y0
    """
    try:
        event.wait
        panel.disable_apt()
        threads=[]
        threads.append(threading.Thread(target=motors[1].move_to,args=(panel.y0,True,)))
        threads.append(threading.Thread(target=motors[0].move_to,args=(panel.x0,True,)))
        for i in threads: i.start()
        for i in threads: i.join()
        panel.x=motors[0].position
        panel.y=motors[1].position
        event.clear()
        panel.enable_apt()
    except Exception as e:
        event.clear()
        panel.enable_apt()
        print(e)
    
    
def find_middle(event,motors,panel):
    """
        Procedura znajdująca środek
    """
    event.wait()
    panel.disable_apt()
    try:
        threads_homne=[]
        threads_homne.append(threading.Thread(target=find_middle_of_one,args=(motors[1],)))
        threads_homne.append(threading.Thread(target=find_middle_of_one,args=(motors[0],)))
        for i in threads_homne:
            i.start()
        for i in threads_homne:
            i.join()
        panel.x=motors[0].position
        panel.x0=motors[0].position
        panel.y=motors[1].position
        panel.y0=motors[1].position
    except Exception as e:
        print(e)
    event.clear()
    panel.enable_apt()

def initialize_motors(event,panel,motors,running):
    """
        Funkcja inicjalizjąca manipulatory APT
    """
    event.wait()
    panel.disable_apt()
    x=None
    try:
        from src import apt
        x = apt.list_available_devices()
        if(x.__len__()<2):
            raise Exception("Nie znaleziono dwóch manipulatorów",x)
            del apt
        print("znaleziono 2 manipulatory",x)
    except Exception as e:
        print(e)
        apt._apt._cleanup()
        del apt
        event.clear()
        panel.enable_apt()
        return None
    motors.append(apt.Motor(x[0][1],0))
    motors.append(apt.Motor(x[1][1],1))
    print("kanał aktywny manipulatora x",motors[0].active_channel)
    print("kanał aktywny manipulatora y",motors[1].active_channel)
    t=threading.Thread(target=monitor_position,args=(motors,panel,running))
    t.start()

    event.clear()
    panel.enable_apt()

def sequence_meassurement(event,event_stop,event_go_to_middle,motors,panel,mapa,mapa2,mapa3,daq_panel,go_to_max):
    """
        procedura pomiarowa
    """
    event.wait()
    #sprawdzanie czy pomiar o danej nazwie już został zapisany w bazie danych
    if check_if_filename_exist(panel.filename,db_name=_db):
        messagebox.showwarning(
            "Open file",
            "Podana nazwa pliku już istnieje \n(%s)" % panel.filename
        )
        exit_sequence(panel,daq_panel,event_stop,event)
        return False

    #blokowanie panelu
    panel.disable()
    daq_panel.disable()
    #deklaracja parametrów pomiaru
    dx = panel.dx
    print("dx",dx)
    dy = panel.dy
    print("dy",dy)
    _x = panel.x_range
    print("_x",_x)
    _y = panel.y_range
    print("_y",_y)
    _x0 = panel.x0
    print("_x0",_x0)
    _y0 = panel.y0
    print("_y0",_y0)
    nx = panel.nx
    print("nx",nx)
    ny = panel.ny
    print("ny",ny)
    points = daq_panel.section_length
    print("points",points)
    average = 0
    print("average",average)
    grid = np.zeros((2*ny,2*nx))
    motor_parallel_move(motors,0,0)
    motor_parallel_move(motors,_x0-_x,_y0-_y)
    #sprawdzenie czy ruchomiono stop
    while(event_stop.is_set()):
                if not event.is_set():
                    exit_sequence(panel,daq_panel,event_stop,event)
                    return False
    print("rozpoczynanie pomiaru")
    #signal.set_shape_freq(panel.points,0.001)
    #ustawianie rozmiarów map
    mapa.set_grid(grid)
    mapa2.set_grid(grid)
    mapa3.set_grid(grid)
    mapa.reshape(2*ny,2*nx,panel.dx,panel.dy,_x,_y)
    mapa2.reshape(2*ny,2*nx,panel.dx,panel.dy,_x,_y)
    mapa3.reshape(2*ny,2*nx,panel.dx,panel.dy,_x,_y)
    #przebieg sekwencji pomiarowej
    for j in range(0,grid.shape[0])[::-1]:
        for i in range(0,grid.shape[1])[::-1]:
            grid=mapa.get_array()
            grid[j][i]=daq_panel.ac #zapisywanie sygnału ac
            mapa.set_grid(grid)
            print("Amp = ",grid[j][i],"[V]") 

            grid=mapa2.get_array()
            grid[j][i]=daq_panel.phase #zapisywanie fazy
            mapa2.set_grid(grid)
            print("Phi = ",grid[j][i],"[°]")

            grid=mapa3.get_array()
            grid[j][i]=daq_panel.ac*np.cos(daq_panel.phase*np.pi/180)
            mapa3.set_grid(grid)
            print("A*cos = ",grid[j][i],"[V]")
            
            motors[0].move_by(dx,blocking = True) #poruszanie się o dx
            while(event_stop.is_set()):
                if not event.is_set():
                    exit_sequence(panel,daq_panel,event_stop,event)
                    return False
        motors[1].move_by(dy,blocking = True) #poruszanie się o dy
        print("dy = ",dy)
        #powrót do początku krawędzi
        motors[0].move_to(_x0-_x+1,True)
        motors[0].move_to(_x0-_x,True)
    panel.enable()
    daq_panel.enable()
    #event.clear()
    panel.start_btn['text']='START'
    panel.start_btn['state']='active'
    panel.stop_btn['text']='PAUSE'
    panel.stop_btn['state']='disable'
    print("Zakonczono proces")
    insert_data(panel.filename,panel.x_range ,panel.y_range ,panel.x_middle ,panel.y_middle ,panel.x_div ,panel.y_div ,panel.m_var.get(),
        1024,40000,"slow",amps=(mapa.get_array(),),phases=(mapa2.get_array(),))
    messagebox.askokcancel("Zakonczono proces", "Zakonczono proces")
    if go_to_max:
        ind = np.unravel_index(np.argmax(grid, axis=None), grid.shape)
        motor_parallel_move(motors,_x0-_x-dx*(2*nx-ind[1]),_y0-_y-dy*(2*ny-ind[0]))
    else:
        go_to_middle(event_go_to_middle,motors,panel)

def lockin(signal,trigger,period,time_const=0.000025):
    #generowanie sygnalowlow referencyjnych
    t_sin = trigger-2.5
    t_sin/=5.0
    shift = int(period/4) #przesuniecie fazowe o pi/2
    t_cos = np.append(t_sin[shift:],t_sin[shift-1::-1])
    #obliczanie parametrow sygnalu
    dc=np.average(signal)
    singal=signal-dc
    y=2*np.average(signal*t_sin)
    x=2*np.average(signal*t_cos)
    ac=np.sqrt(x*x+y*y)#*np.sqrt(2)/2.0
    phase=np.arctan2(x,y)*180/np.pi
    return round(dc,4), round(ac,4), round(phase,4), round(1/(period)/time_const,4)

def fast_meassurement(event,event_stop,event_go_to_middle,motors,panel,mapa,mapa2,mapa3,daq_panel,go_to_max,daq,call_back):
    #event.wait()
    try:
        if check_if_filename_exist(panel.filename,db_name=_db):
            messagebox.showwarning(
                "Open file",
                "Podana nazwa pliku już istnieje \n(%s)" % panel.filename
            )
            exit_sequence(panel,daq_panel,event_stop,event)
            return False

        #blokowanie panelu
        panel.disable()
        daq_panel.disable()
        dx = panel.dx
        dy = panel.dy
        _x = panel.x_range
        _y = panel.y_range
        _x0 = panel.x0
        _y0 = panel.y0
        nx = panel.nx
        ny = panel.ny
        points = daq_panel.section_length
        print("points ",points,"ny ",ny,"nx ",nx,"_y0 ",_y0,"_x0 ",_x0,"_y ",_y,"_x ",_x,"dy ",dy,"dx ",dx)

        M=float(panel.m_var.get())+1.0
        fp=40e3
        print("pomiar częstotliwości choppera...")
        daq.getData()
        idx=[i for i,x in enumerate(np.diff(daq.get_ref())) if x>1] # znajdowanie zboczy narastających
        idx=[y for x,y in zip(np.diff(idx),idx[:-1]) if x>3]
        fc=1/np.average(np.diff(idx))/0.000025
        buffor_len=int(2*_x*(M)*fp/fc/dx)
        N=int((M)*fp/fc) #ilosc probek na dx
        K=int(buffor_len/N) #ilosc dzialek mapy
        v = fc*dx/(M)
        distance = v*v/2.9/2
        print("fc=",fc,"fp=",fp,"v= ",v,"buffor_len= ",buffor_len,"N= ",N,"K= ",K)

        grid = np.zeros((2*ny,2*nx))
        motor_parallel_move(motors,0,0)
        motor_parallel_move(motors,_x0-_x-distance,_y0-_y)

        daq.reinit(section_length=buffor_len,call_back=call_back)
        #sprawdzenie czy ruchomiono stop
        while(event_stop.is_set()):
                    if not event.is_set():
                        exit_sequence(panel,daq_panel,event_stop,event)
                        daq.reinit(section_length=1024,call_back=call_back)
                        return False

        print("rozpoczynanie pomiaru")
        print(motors[0].get_velocity_parameters())

        motors[0].set_velocity_parameters(
            0,
            2.9,
            v
        )

        print(motors[0].get_velocity_parameters())
        #signal.set_shape_freq(panel.points,0.001)
        #ustawianie rozmiarów map
        mapa.set_grid(grid)
        mapa2.set_grid(grid)
        mapa3.set_grid(grid)
        mapa.reshape(2*ny,2*nx,panel.dx,panel.dy,_x,_y)
        mapa2.reshape(2*ny,2*nx,panel.dx,panel.dy,_x,_y)
        mapa3.reshape(2*ny,2*nx,panel.dx,panel.dy,_x,_y)
        #przebieg sekwencji pomiarowej
        for j in range(0,grid.shape[0])[::-1]:
            amp = []
            phase = []
            thread = threading.Thread(target=motors[0].move_to,args=(_x0+_x+distance,True,))
            thread.start()
            thread2 = threading.Thread(target=daq.getData,args=())
            while(motors[0].position<_x0-_x):
                pass
            thread2.start()
            thread2.join()
            for i in range(0,K):
                sig = daq.get_sig()[i*N:(i+1)*N]
                ref = daq.get_ref()[i*N:(i+1)*N]
                idx=[i for i,x in enumerate(np.diff(ref)) if x>1] # znajdowanie zboczy narastających
                idx=[y for x,y in zip(np.diff(idx),idx[:-1]) if x>3]
                sig = sig[idx[0]:idx[-1]]
                ref = ref[idx[0]:idx[-1]]
                #print("idx")
                ref_dc = np.average(ref)
                ref = (ref-2.5)/2.5
                shift = int(np.average(np.diff(idx))/4)
                y=2*np.average(sig*ref)
                x=2*np.average(sig*np.append(ref[shift:],ref[shift-1::-1]))
                ac=np.sqrt(x*x+y*y)
                ph=np.arctan2(x,y)*180/np.pi
                amp.append(ac)
                phase.append(ph)

            grid = mapa.get_array()
            grid[j,:]=amp
            mapa.set_grid(grid)

            grid = mapa2.get_array()
            grid[j,:]=phase
            mapa2.set_grid(grid)

            grid = mapa3.get_array()
            grid[j,:]=amp*np.cos(np.asarray(phase)*np.pi/180)
            mapa3.set_grid(grid)

            motors[0].set_velocity_parameters(0,2.9,1.5)
            motors[0].move_to(_x0-_x-distance,True)
            motors[1].move_by(dy,True)
            print("end = ", motors[0].position)
            motors[0].set_velocity_parameters(0,2.9,v)
            while(event_stop.is_set()):
                if not event.is_set():
                    exit_sequence(panel,daq_panel,event_stop,event)
                    daq.reinit(section_length=1024,call_back=call_back)
                    motors[0].set_velocity_parameters(0,2.9,1.5)
                    return False
        panel.enable()
        daq_panel.enable()
        motors[0].set_velocity_parameters(0,2.9,1.5)
        #event.clear()
        panel.start_btn['text']='START'
        panel.start_btn['state']='active'
        panel.stop_btn['text']='PAUSE'
        panel.stop_btn['state']='disable'
        print("Zakonczono proces")
        insert_data(panel.filename,panel.x_range ,panel.y_range ,panel.x0 ,panel.y0 ,panel.nx ,panel.ny ,panel.m_var.get(),
        1024,40000,"fast_dq",amps=(mapa.get_array(),),phases=(mapa2.get_array(),))
        messagebox.askokcancel("Zakonczono proces", "Zakonczono proces")
        if go_to_max:
            ind = np.unravel_index(np.argmax(grid, axis=None), grid.shape)
            print(ind)
            motor_parallel_move(motors,_x0-_x-dx*(2*nx-ind[1]),_y0-_y-dy*(2*ny-ind[0]))
        else:
            go_to_middle(event_go_to_middle,motors,panel)
        daq.reinit(section_length=1024,call_back=call_back)
    except Exception as e:
        print(e)
        exit_sequence(panel,daq_panel,event_stop,event)
        daq.reinit(section_length=1024,call_back=call_back)
        motors[0].set_velocity_parameters(0,2.9,1.5)

def fast_meassurement_4ch(event,event_stop,event_go_to_middle,motors,panel,amp_maps,ph_maps,daq_panel,go_to_max,daq,call_back):
    #event.wait()
    try:
        if check_if_filename_exist(panel.filename,db_name=_db):
            messagebox.showwarning(
                "Open file",
                "Podana nazwa pliku już istnieje \n(%s)" % panel.filename
            )
            exit_sequence(panel,daq_panel,event_stop,event)
            return False

        #blokowanie panelu
        panel.disable()
        daq_panel.disable()
        dx = panel.dx
        dy = panel.dy
        _x = panel.x_range
        _y = panel.y_range
        _x0 = panel.x0
        _y0 = panel.y0
        nx = panel.nx
        ny = panel.ny
        points = daq_panel.section_length
        print("points ",points,"ny ",ny,"nx ",nx,"_y0 ",_y0,"_x0 ",_x0,"_y ",_y,"_x ",_x,"dy ",dy,"dx ",dx)

        M=float(panel.m_var.get())+1.0
        fp=40e3
        print("pomiar częstotliwości choppera...")
        daq.getData()
        idx=[i for i,x in enumerate(np.diff(daq.get_ref())) if x>1] # znajdowanie zboczy narastających
        idx=[y for x,y in zip(np.diff(idx),idx[:-1]) if x>3]
        fc=1/np.average(np.diff(idx))/0.000025
        buffor_len=int(2*_x*(M)*fp/fc/dx)
        N=int((M)*fp/fc) #ilosc probek na dx
        K=int(buffor_len/N) #ilosc dzialek mapy
        v = fc*dx/(M)
        distance = v*v/2.9/2
        print("fc=",fc,"fp=",fp,"v= ",v,"buffor_len= ",buffor_len,"N= ",N,"K= ",K)

        grid = np.zeros((2*ny,2*nx))
        motor_parallel_move(motors,0,0)
        motor_parallel_move(motors,_x0-_x-distance,_y0-_y)

        daq.reinit(section_length=buffor_len,call_back=call_back)
        #sprawdzenie czy ruchomiono stop
        while(event_stop.is_set()):
                    if not event.is_set():
                        exit_sequence(panel,daq_panel,event_stop,event)
                        daq.reinit(section_length=1024,call_back=call_back)
                        return False

        print("rozpoczynanie pomiaru")
        print(motors[0].get_velocity_parameters())

        motors[0].set_velocity_parameters(
            0,
            2.9,
            v
        )

        print(motors[0].get_velocity_parameters())
        #signal.set_shape_freq(panel.points,0.001)
        #ustawianie rozmiarów map
        for mapa in amp_maps:
            mapa.set_grid(grid)
            mapa.reshape(2*ny,2*nx,panel.dx,panel.dy,_x,_y)
        for mapa in ph_maps:
            mapa.set_grid(grid)
            mapa.reshape(2*ny,2*nx,panel.dx,panel.dy,_x,_y)
        #przebieg sekwencji pomiarowej
        for j in range(0,grid.shape[0])[::-1]:
            amp = []
            phase = []
            thread = threading.Thread(target=motors[0].move_to,args=(_x0+_x+distance,True,))
            thread.start()
            thread2 = threading.Thread(target=daq.getData,args=())
            while(motors[0].position<_x0-_x):
                pass
            thread2.start()
            thread2.join()
            for i in range(0,K):
                signals=[]
                for k in np.arange(1,5)[::-1]:
                    signals.append(
                        daq.get_sig(k)[i*N:(i+1)*N]
                    )
                ref = daq.get_ref()[i*N:(i+1)*N]
                idx=[i for i,x in enumerate(np.diff(ref)) if x>1] # znajdowanie zboczy narastających
                idx=[y for x,y in zip(np.diff(idx),idx[:-1]) if x>3]
                for k in range(0,signals.__len__()):
                    signals[k]=signals[k][idx[0]:idx[-1]]
                ref = ref[idx[0]:idx[-1]]
                #print("idx")
                ref = (ref-2.5)/2.5
                shift = int(np.average(np.diff(idx))/4)
                ac = []
                ph = []
                for sig in signals:
                    y=2*np.average(sig*ref)
                    x=2*np.average(sig*np.append(ref[shift:],ref[shift-1::-1]))
                    ac.append(np.sqrt(x*x+y*y))
                    ph.append(np.arctan2(x,y)*180/np.pi)
                amp.append(ac)
                phase.append(ph)
                
            phase =np.asarray(phase)
            amp = np.asarray(amp)

            #rysowanie map
            for m in range(0,amp_maps.__len__()):
                grid = amp_maps[m].get_array()
                grid[j,:]=amp[:,m]
                amp_maps[m].set_grid(grid)

                grid = ph_maps[m].get_array()
                grid[j,:]=phase[:,m]
                ph_maps[m].set_grid(grid)

            motors[0].set_velocity_parameters(0,2.9,1.5)
            motors[0].move_to(_x0-_x-distance,True)
            motors[1].move_by(dy,True)
            print("end = ", motors[0].position)
            motors[0].set_velocity_parameters(0,2.9,v)
            while(event_stop.is_set()):
                if not event.is_set():
                    exit_sequence(panel,daq_panel,event_stop,event)
                    daq.reinit(section_length=1024,call_back=call_back)
                    motors[0].set_velocity_parameters(0,2.9,1.5)
                    return False
        panel.enable()
        daq_panel.enable()
        motors[0].set_velocity_parameters(0,2.9,1.5)
        #event.clear()
        panel.start_btn['text']='START'
        panel.start_btn['state']='active'
        panel.stop_btn['text']='PAUSE'
        panel.stop_btn['state']='disable'
        print("Zakonczono proces")
        insert_data(panel.filename,panel.x_range ,panel.y_range ,panel.x0 ,panel.y0 ,panel.nx ,panel.ny ,panel.m_var.get(),
        1024,40000,"fast_dq",amps=(amp_maps[0].get_array(),amp_maps[1].get_array(),amp_maps[2].get_array(),amp_maps[3].get_array(),),
        phases=(ph_maps[0].get_array(),ph_maps[1].get_array(),ph_maps[2].get_array(),ph_maps[3].get_array(),))
        messagebox.askokcancel("Zakonczono proces", "Zakonczono proces")
        go_to_middle(event_go_to_middle,motors,panel)
        daq.reinit(section_length=1024,call_back=call_back)
    except Exception as e:
        print(e)
        exit_sequence(panel,daq_panel,event_stop,event)
        daq.reinit(section_length=1024,call_back=call_back)
        motors[0].set_velocity_parameters(0,2.9,1.5)
        raise(e)


def sequence_meassurement_nanovoltometer(event,event_stop,event_go_to_middle,motors,panel,mapa,mapa2,daq_panel):
    """
        procedura pomiarowa
    """
    event.wait()
    #sprawdzanie czy pomiar o danej nazwie już został zapisany w bazie danych
    if check_if_filename_exist(panel.filename,db_name=_db):
        messagebox.showwarning(
            "Open file",
            "Podana nazwa pliku już istnieje \n(%s)" % panel.filename
        )
        exit_sequence(panel,daq_panel,event_stop,event)
        return False

    #blokowanie panelu
    panel.disable()
    daq_panel.disable()
    #deklaracja parametrów pomiaru
    dx = panel.dx
    print("dx",dx)
    dy = panel.dy
    print("dy",dy)
    _x = panel.x_range
    print("_x",_x)
    _y = panel.y_range
    print("_y",_y)
    _x0 = panel.x0
    print("_x0",_x0)
    _y0 = panel.y0
    print("_y0",_y0)
    nx = panel.nx
    print("nx",nx)
    ny = panel.ny
    print("ny",ny)
    points = daq_panel.section_length
    print("points",points)
    average = 0
    print("average",average)
    grid = np.zeros((2*ny,2*nx))
    grid2 = np.zeros((2*ny,2*nx))
    grid3 = np.zeros((2*ny,2*nx))
    motor_parallel_move(motors,0,0)
    motor_parallel_move(motors,_x0-_x,_y0-_y)
    #sprawdzenie czy ruchomiono stop
    while(event_stop.is_set()):
                if not event.is_set():
                    exit_sequence(panel,daq_panel,event_stop,event)
                    return False
    print("rozpoczynanie pomiaru")
    #signal.set_shape_freq(panel.points,0.001)
    #ustawianie rozmiarów map
    mapa.set_grid(grid)
    mapa2.set_grid(grid2)
    mapa.reshape(2*ny,2*nx,panel.dx,panel.dy,_x,_y)
    mapa2.reshape(2*ny,2*nx,panel.dx,panel.dy,_x,_y)
    #przebieg sekwencji pomiarowej
    for j in range(0,grid.shape[0])[::-1]:
        for i in range(0,grid.shape[1])[::-1]:
            grid[j][i]=abs(daq_panel.dc)
            if(daq_panel.dc>0):
                grid2[j][i]=1 #zapisywanie fazy
            else:
                grid2[j][i]=-1
            print(grid[j][i],"[V]") 
            print(grid2[j][i],"[°]")
            mapa.set_grid(grid)
            mapa2.set_grid(grid2)
            print(dx)
            motors[0].move_by(dx,blocking = True) #poruszanie się o dx
            while(event_stop.is_set()):
                if not event.is_set():
                    exit_sequence(panel,daq_panel,event_stop,event)
                    return False
        motors[1].move_by(dy,blocking = True) #poruszanie się o dy
        print("dy = ",dy)
        #powrót do początku krawędzi
        motors[0].move_to(_x0-_x+1,True)
        motors[0].move_to(_x0-_x,True)
    panel.enable()
    daq_panel.enable()
    #event.clear()
    panel.start_btn['text']='START'
    panel.start_btn['state']='active'
    panel.stop_btn['text']='PAUSE'
    panel.stop_btn['state']='disable'
    print("Zakonczono proces")
    insert_data(panel.filename,panel.x_range ,panel.y_range ,panel.x_middle ,panel.y_middle ,panel.x_div ,panel.y_div ,panel.m_var.get(),
    buffor_len,40000,"nano",amps=(grid,),phases=(grid2,))
    messagebox.askokcancel("Zakonczono proces", "Zakonczono proces")
    go_to_middle(event_go_to_middle,motors,panel)



def add_all(signals):
    """
        funkcja uśredniająca bufor sygnałów, raczej nie używana ale może kiedyś się przyda
    """
    result=signals[0]
    for i in range(1,len(signals)):
        if len(result)>len(signals[i]):
            result+=np.concatenate((signals[i],np.zeros(len(result)-len(signals[i]))))
        else:
            pass
            result=np.concatenate((result,np.zeros(int(len(signals[i])-len(result)))))+signals[i]
    return result/len(signals)


def cont_meassurement(event,event_stop,event_go_to_middle,motors,panel,mapa,mapa2,mapa3,daq_panel,go_to_max,daq):
    """
        procedura pomiarowa
    """
    event.wait()
    dx = panel.dx
    print("dx",dx)
    dy = panel.dy
    print("dy",dy)
    _x = panel.x_range
    print("_x",_x)
    _y = panel.y_range
    print("_y",_y)
    _x0 = panel.x0
    print("_x0",_x0)
    _y0 = panel.y0
    print("_y0",_y0)
    nx = panel.nx
    print("nx",nx)
    ny = panel.ny
    print("ny",ny)
    points = daq_panel.section_length
    print("points",points)
    average = 0
    print("average",average)
    grid = np.zeros((2*ny,2*nx))
    motor_parallel_move(motors,0,0)
    motors[1].move_to(_y0,True)
    motors[0].move_to(_x0+_x)
    while(motors[0].position<_x0-_x):
        pass
    daq.getData()
    while(motors[0].position>_x0+_x):
        pass
    #plt.plot(daq.get_sig())
    #plt.show()
    np.savetxt("dane.csv", daq.get_sig(), fmt='%.18e', delimiter=',', newline='\n')
    go_to_middle(event_go_to_middle,motors,panel)

def line_messurement(event,event_stop,event_go_to_middle,motors,panel,mapa,mapa2,mapa3,daq_panel,go_to_max):
    """
        procedura pomiarowa
    """
    event.wait()
    #sprawdzanie czy pomiar o danej nazwie już został zapisany w bazie danych
    if check_if_filename_exist(panel.filename,db_name=_db):
        messagebox.showwarning(
            "Open file",
            "Podana nazwa pliku już istnieje \n(%s)" % panel.filename
        )
        exit_sequence(panel,daq_panel,event_stop,event)
        return False

    #blokowanie panelu
    panel.disable()
    daq_panel.disable()
    #deklaracja parametrów pomiaru
    dx = panel.dx
    print("dx",dx)
    dy = panel.dy
    print("dy",dy)
    _x = panel.x_range
    print("_x",_x)
    _y = panel.y_range
    print("_y",_y)
    _x0 = panel.x0
    print("_x0",_x0)
    _y0 = panel.y0
    print("_y0",_y0)
    nx = panel.nx
    print("nx",nx)
    ny = panel.ny
    print("ny",ny)
    points = daq_panel.section_length
    print("points",points)
    average = 0
    print("average",average)
    grid = np.zeros((2*ny,2*nx))
    data = np.zeros(2*nx)
    motor_parallel_move(motors,0,0)
    motor_parallel_move(motors,_x0-_x,_y0)
    #sprawdzenie czy ruchomiono stop
    while(event_stop.is_set()):
                if not event.is_set():
                    exit_sequence(panel,daq_panel,event_stop,event)
                    return False
    print("rozpoczynanie pomiaru")
    #signal.set_shape_freq(panel.points,0.001)
    #ustawianie rozmiarów map
    mapa.set_grid(grid)
    mapa2.set_grid(grid)
    mapa3.set_grid(grid)
    mapa.reshape(2*ny,2*nx,panel.dx,panel.dy,_x,_y)
    mapa2.reshape(2*ny,2*nx,panel.dx,panel.dy,_x,_y)
    mapa3.reshape(2*ny,2*nx,panel.dx,panel.dy,_x,_y)
    #przebieg sekwencji pomiarowej
    for j in range(0,2*nx)[::-1]:
        grid=mapa.get_array()
        data[j]=daq_panel.ac
        grid[ny][j]=daq_panel.ac #zapisywanie sygnału ac
        mapa.set_grid(grid)
        print("Amp = ",grid[ny][j],"[V]") 
        motors[0].move_by(dx,blocking = True) #poruszanie się o dx
        print("dx = ",dx)
        while(event_stop.is_set()):
            if not event.is_set():
                exit_sequence(panel,daq_panel,event_stop,event)
                return False
    panel.enable()
    daq_panel.enable()
    #event.clear()
    panel.start_btn['text']='START'
    panel.start_btn['state']='active'
    panel.stop_btn['text']='PAUSE'
    panel.stop_btn['state']='disable'
    #show_plot(data)
    print("Zakonczono proces")
    insert_data(panel.filename,panel.x_range ,panel.y_range ,panel.x_middle ,panel.y_middle ,panel.x_div ,panel.y_div ,panel.m_var.get(),
        1024,40000,"line",amps=(mapa.get_array(),),phases=(mapa2.get_array(),))
    show_plot(data)
    messagebox.askokcancel("Zakonczono proces", "Zakonczono proces")
    if go_to_max:
        ind = np.unravel_index(np.argmax(grid, axis=None), grid.shape)
        motor_parallel_move(motors,_x0-_x+dx*(2*nx-ind[1]),_y0-_y+dy*(2*ny-ind[0]))
    else:
        go_to_middle(event_go_to_middle,motors,panel)


def display_data(number,panel,mapa,mapa2):
    (info,labels,amps,phases)=get_data(number)
    mapa.reshape(2*info[8],2*info[7],info[3]/info[7],info[4]/info[8],info[3],info[4])
    mapa2.reshape(2*info[8],2*info[7],info[3]/info[7],info[4]/info[8],info[3],info[4])
    mapa2.set_grid(phases[0])
    print(np.shape(amps))
    mapa.set_grid(amps[0])
    panel.filename=info[2]
    panel.x_range=info[3]
    panel.y_range=info[4]
    panel.nx=info[7]
    panel.ny=info[8]
    panel.m=info[9]
    panel.x0=info[5]
    panel.y0=info[6]



def show_plot(data):
    fig, ax = plt.subplots()
    ax.plot(data)
    fig.show()
