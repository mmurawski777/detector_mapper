"""
    Moduł przechowuje obiekt UI wyświetlający parametry pomiaru sygnału i wartości zmierzone przez lockina
    czyli faze amplitude częstotliwośc
"""
from tkinter import *

class DaqPanel(object):
    def __init__(self,root,column,row,re_init_daq=None,switch=None):
        """
            Konstruktor
            root - root tkinter
            column - kolumna
            row - rząd
            sampling_rate - stała potrzebna do przeliczania częstotliwości i czasu akwzycji przez suwak
            re_init_daq - funkcja do reinicjalizacji obiektu obsługującego kartę pomiarową np. przy zmianie długości buffora
            switch - funkcja do reinicjalizacji obiektu obsługującego kartę pomiarową np. przy wykorzystaniu nanowoltomierza
        """
        self.column=column
        self.row=row
        self.option_mnu_var = StringVar()
        self.option_mnu_var.set("DQ slow measurement")
        self.option_mnu= OptionMenu(root, self.option_mnu_var, "DQ slow measurement","DQ fast measurement","DQ fast measurement 4CH","DQ 1D measurement", "NV",command=switch)
        self.option_mnu.config(width=20)
        self.option_mnu.grid(column=self.column,row=self.row,sticky="NW")
        self.dc_var=StringVar()
        self.dc_var.set("DC   [V] =0")
        self.dc_lbl = Label(root,textvariable=self.dc_var)
        self.dc_lbl.grid(column=self.column,row=self.row+1,sticky="NW")
        self.ac_var=StringVar()
        self.ac_var.set("AC   [V] =0")
        self.ac_lbl = Label(root,textvariable=self.ac_var)
        self.ac_lbl.grid(column=self.column,row=self.row+2,sticky="NW")
        self.phase_var=StringVar()
        self.phase_var.set("faza [°] =0")
        self.phase_lbl = Label(root,textvariable=self.phase_var)
        self.phase_lbl.grid(column=self.column,row=self.row+3,sticky="NW")
        self.freq_var=StringVar()
        self.freq_var.set("f       [Hz] =0")
        self.freq_lbl = Label(root,textvariable=self.freq_var)
        self.freq_lbl.grid(column=self.column,row=self.row+4,sticky="NW")
        self.re_init_daq=re_init_daq

    
        
    def btn_fun(self,*args):
        self.re_init_daq()
    @property
    def section_length(self):
        return int(1024)
    @property
    def ac(self):
        return float(self.ac_var.get()[10:])
    @ac.setter
    def ac(self,x):
        self.ac_var.set("AC   [V] ="+str(x))
    @property
    def dc(self):
        return float(self.dc_var.get()[10:])
    @dc.setter
    def dc(self,x):
        self.dc_var.set("DC   [V] ="+str(x))
    @property
    def phase(self):
        return float(self.phase_var.get()[11:])
    @phase.setter
    def phase(self,x):
        self.phase_var.set("phase [°] ="+str(x))
    @property
    def freq(self):
        return float(self.freq_var.get())
    @phase.setter
    def freq(self,x):
        self.freq_var.set("f       [Hz] ="+str(x))

    def disable(self):
        self.option_mnu['state']='disabled'
        
    def enable(self):
        self.option_mnu['state']='normal'

    def set_parameters(self,dc,ac,phase,freq):
        self.dc=dc
        self.ac=ac
        self.phase=phase
        self.freq=freq


def test():
    root = Tk()
    daq = Usb4716()
    def aqq():
        while(True):
            daq.getData()
            data=daq.getBuffer()
            #print(data)
            messurement_panel.compute_parameters(data)
    

    messurement_panel = DaqPanel(root,0,0)
    messurement_panel.osc=False
    messurement_panel.ac=1
    messurement_panel.dc=1
    messurement_panel.phase=1
    messurement_panel.disable()
    messurement_panel.enable()
    threading.Thread(target=aqq).start()
    root.mainloop()

