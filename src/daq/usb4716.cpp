#include "compatibility.h"
#include "bdaqctrl.h"
#include <iostream>
#include <windows.h>
using namespace Automation::BDaq;

#define min(a,b) (((a)<(b))?(a):(b))
#define max(a,b) (((a)>(b))?(a):(b))

class Usb4716{
public:
    ErrorCode ret;
    int32 returnedCount=0;
    WaveformAiCtrl* wfAiCtrl;
    const wchar_t* deviceDescription;
    const wchar_t* profilePath;
    int32 startChannel,channelCount,sectionLength,sectionCount;
    double* userDataBuffer;
    Conversion* conversion;
    Record* record;
    Usb4716();
    Usb4716(const wchar_t* deviceDescription,const wchar_t* profilePath,
        int32 startChannel,int32 channelCount,int32 sectionLength,int32 sectionCount,void(*call_back)(void));
    void start();
    void stop();
    ~Usb4716();
    void getData();
    void printBuffer();
    double * getBuffer();
    void(*call_back)(void);
};

void BDAQCALL OnStoppedEvent(void * sender, BfdAiEventArgs * args, void *userParam)
{
}

Usb4716::Usb4716(const wchar_t* deviceDescription,const wchar_t* profilePath,
    int32 startChannel,int32 channelCount,int32 sectionLength,int32 sectionCount,void(*call_back)(void)){
    this->deviceDescription=deviceDescription;
    this->profilePath=profilePath;
    this->startChannel=startChannel;
    this->channelCount=channelCount;
    this->sectionLength=sectionLength;
    this->call_back=call_back;
    userDataBuffer = new double[channelCount*sectionLength];
    sectionCount=sectionCount;
    ret = Success;
    returnedCount = 0;
    
    DeviceInformation devInfo(deviceDescription);
    wfAiCtrl = WaveformAiCtrl::Create();
    ret = wfAiCtrl->setSelectedDevice(devInfo);
    ret = wfAiCtrl->LoadProfile(profilePath);
    conversion = wfAiCtrl->getConversion();
    ret = conversion->setChannelStart(startChannel);
    ret = conversion->setChannelCount(channelCount);
    record = wfAiCtrl->getRecord();
    ret = record->setSectionLength(sectionLength);
    ret = record->setSectionCount(sectionCount);
    ret = wfAiCtrl->Prepare();
    //ret = wfAiCtrl->Start();
}

Usb4716::~Usb4716(){
    ret = wfAiCtrl->Stop();
    wfAiCtrl->Dispose();
}

void Usb4716::getData(void){
    
    ret = wfAiCtrl->Start();
    //std::cout<<"returned cout= "<<returnedCount<<std::endl;
    ret = wfAiCtrl->GetData(channelCount*sectionLength, userDataBuffer, -1, &returnedCount);
    //while(returnedCount<sectionLength*sectionCount){
    //std::cout<<"returned cout= "<<returnedCount<<std::endl;
    returnedCount=0;
    ret = wfAiCtrl->Stop();
    //wfAiCtrl->Dispose();
    call_back();

    
}
void Usb4716::printBuffer(){
    std::cout<<sectionLength*channelCount<<std::endl;
    for(int jj=0;jj<channelCount*sectionLength;jj++) std::cout<<userDataBuffer[jj]<<"\t";
}

double* Usb4716::getBuffer(){
    return userDataBuffer;
}


extern "C"
{
    //Usb4716* Usb4716_new(void) {return new Usb4716();}
    Usb4716* Usb4716_new(const wchar_t* deviceDescription,const wchar_t* profilePath,
        int32 startChannel,int32 channelCount,int32 sectionLength,int32 sectionCount,void(*call_back)(void)) {
            return new Usb4716(deviceDescription,profilePath,startChannel,channelCount,sectionLength,sectionCount,call_back);}
    void Usb4716_getData(Usb4716* foo) {foo->getData();}
    void Usb4716_printBuffer(Usb4716* foo) {foo->printBuffer();}
    void Usb4716_delete(Usb4716* foo) {delete foo;}
    double* Usb4716_getBuffer(Usb4716* foo){foo->getBuffer();}
}