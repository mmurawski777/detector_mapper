
import ctypes
from numpy.ctypeslib import ndpointer
import numpy as np
import matplotlib.pylab as plt
#from scipy import signal
import os
"""@package docstring
    ModuĹ‚ realizujÄ…cy obsĹ‚ugÄ™ karty pomiarowej w pythonie
    Skompilowana biblioteka dynamiczna .dll przechowuje sterowanie kartÄ… pomiarowÄ…
    Python realizuje interfejs obiektu w pliku .dlll
"""
_dir = os.getcwd()
_dll = _dir+"\\src\\daq\\usb4716.dll"
_xml = _dir+"\\src\\daq\\4761_ch15_14_20k.xml"
#_xml = _dir+"\\4761_ch15_14_20k.xml"
_xml2 = _dir+"\\src\\daq\\nanovoltometer.xml"
print(_dll)
print(_xml)
lib = ctypes.cdll.LoadLibrary(_dll)

class Usb4716(object):
    def __init__(self,device_description="USB-4716,BID#1",profile_path=_xml,
    start_channel=11,channel_count=5,section_length=1024,section_count=0,call_back=None):
        """
            Konstruktor obiektu
            Args:
                device_description - opis urzÄ…dzenia
                profile_path - plik profilu urzÄ…dzenia - sÄ… tam zdefiniowane bardziej szczegĂłĹ‚owe parametry
                start_channel - kanaĹ‚ od ktĂłrego zaczynamy mierzyÄ‡
                channel_count - iloĹ›Ä‡ mierzonych kanaĹ‚ow
                section_length - dĹ‚ugoĹ›Ä‡ sekcji (dĹ‚ugoĹ›Ä‡ buffora = channel_cout*section_length)
                section_count - iloĹ›c mierzonych dodatkowych sekcji
        """
        print(profile_path)
        with open(_xml, 'r') as file:
            data = file.readlines()
        data[83]="<Value>"+str(int(section_length))+"</Value>\n"
        with open(_xml, 'w') as file:
            file.writelines(data)
        self.device_type="DQ"
        self.buffer_size=section_length*channel_count
        lib.Usb4716_new.argtypes = [ctypes.c_wchar_p,ctypes.c_wchar_p,
        ctypes.c_int32,ctypes.c_int32,ctypes.c_int32,ctypes.c_int32,ctypes.CFUNCTYPE(None)]
        lib.Usb4716_new.restype = ctypes.c_void_p
        lib.Usb4716_getData.argtypes = [ctypes.c_void_p]
        lib.Usb4716_getData.restype = ctypes.c_void_p
        lib.Usb4716_delete.argtypes = [ctypes.c_void_p]
        lib.Usb4716_delete.restype = ctypes.c_void_p
        lib.Usb4716_printBuffer.argtypes = [ctypes.c_void_p]
        lib.Usb4716_printBuffer.restype = ctypes.c_void_p
        lib.Usb4716_getBuffer.argtypes = [ctypes.c_void_p]
        lib.Usb4716_getBuffer.restype = ndpointer(dtype=ctypes.c_double, shape=(section_length*channel_count,))
        #c_call_back = ctypes.CFUNCTYPE(None,ctypes.c_void_p)(call_back)
        self.obj = lib.Usb4716_new(device_description,profile_path,start_channel,channel_count,section_length,section_count,call_back)
        #self.thread=multiprocessing.Process(target=self.getDataLoop)
        #self.thread.start()


    def __del__(self):
        """
            Destruktor
        """
        lib.Usb4716_delete(self.obj)
    
    def nanovolotmeter(self,section_length=1024,call_back=None):
        """
            Funkcja zmieniajÄ…ca domyĹ›lne parametry karty pomiarowej na parametry do wykonywania pomiaru
            przy wykorzystaniu nanowoltomierza
        """
        print(_xml2)
        self.__del__()
        with open(_xml2, 'r') as file:
            data = file.readlines()
        data[83]="<Value>"+str(int(section_length))+"</Value>\n"
        with open(_xml2, 'w') as file:
            file.writelines(data)
        self.__init__(device_description="USB-4716,BID#1",profile_path=_xml2,start_channel=14,channel_count=2,section_length=section_length,section_count=0,call_back=call_back)
        self.device_type="NV"
    
    def reinit(self,section_length=1024,call_back=None):
        """
            Funkcja zmieniajÄ…ca domyĹ›lne parametry karty pomiarowej na parametry do wykonywania pomiaru
            przy wykorzystaniu nanowoltomierza
        """
        print(_xml)
        self.__del__()
        with open(_xml, 'r') as file:
            data = file.readlines()
        data[83]="<Value>"+str(int(section_length))+"</Value>\n"
        with open(_xml, 'w') as file:
            file.writelines(data)
        self.__init__(device_description="USB-4716,BID#1",profile_path=_xml,start_channel=11,channel_count=5,section_length=section_length,section_count=0,call_back=call_back)

    def getData(self):
        """
            Fukcja zbierajÄ…ca buffor danych
        """
        lib.Usb4716_getData(self.obj)
    
    #def getTestBuffer(self):
    #    """
    #        Funkcja do testowania bez sygnaĹ‚u
    #    """
    #    b=-signal.square(np.linspace(0,20*np.pi,self.buffer_size/2))*1.5+1.5#+np.random.random(int(self.buffer_size/2))
    #    a=signal.square(np.linspace(0,20*np.pi,self.buffer_size/2)+np.random.random()/10)*1.5 +1.5
    #    result = [ item for tup in zip(a,b) for item in tup ]
    #    return result
    def getBuffer(self):
        """
            Funkcja zwraca bufor danych
        """
        return lib.Usb4716_getBuffer(self.obj)

    def get_sig(self,number=4):
        return lib.Usb4716_getBuffer(self.obj)[number::5]

    def get_ref(self):
        return lib.Usb4716_getBuffer(self.obj)[0::5]

@ctypes.CFUNCTYPE(None)
def done():
    print("done")
    pass

def test():
    daq = Usb4716(call_back=done)
    daq.getData()
    ref = daq.get_ref()
    sig = daq.get_sig()
    idx=[i for i,x in enumerate(np.diff(ref)) if x>1] # znajdowanie zboczy narastających
    idx=[y for x,y in zip(np.diff(idx),idx[:-1]) if x>3]
    print(np.diff(idx))
    print("buffer: ", ref.__len__())
    plt.plot(ref[idx[0]:idx[-1]])
    #plt.plot(daq.get_sig())
    #plt.plot(daq.get_ref())
    plt.show()
    



    
    del daq

if __name__=="__main__":
    test()