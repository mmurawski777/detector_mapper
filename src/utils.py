import pickle

def save_obj(obj, name ):
     """
        Funkcja zapisująca słownik obj w pliku o naziwe name
            Args:
                obj - obiekt słownika
                name - nazwa pliku .cfg 
    """
     with open(name, 'wb') as f:
          pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)

def load_obj(name ):
    """
        Funkcja odczytująca słownik obj w pliku o naziwe name
        Args:
            obj - obiekt słownika
            name - nazwa pliku .cfg
        Return:
            słownik 
    """
    with open(name, 'rb') as f:
        return pickle.load(f)



options ={
    "img_dir":"",
    "csv_dir":""
}
