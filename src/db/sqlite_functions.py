"""
    Moduł przechowuje funkcje do zarzadzania bazą danych
"""
import sqlite3
import os
import numpy as np
_dir = os.getcwd()
_db = _dir+"\\src\\db\\test.db"
print("_db",_db)
import matplotlib.pylab as plt

def get_connection_to_db(db_name=_db):
    """
        Połącz z bazą danych
        Args:
            db_name= lokalizacjia pliku db
    """
    return sqlite3.connect(db_name)


def create_table_map_info(db_name=_db):
    """
        Stworzenie tablicy map_info
        Args:
            db_name= lokalizacjia pliku db
    """
    try:
        _connection = get_connection_to_db(db_name)
        _connection.cursor().execute("""
        CREATE TABLE map_info(
            info_id INTEGER PRIMARY KEY AUTOINCREMENT,
            data_zam timestamp DEFAULT CURRENT_TIMESTAMP,
            filename TEXT,
            x_range FLOAT,
            y_range FLOAT,
            x_middle FLOAT,
            y_middle FLOAT,
            x_div INTEGER,
            y_div INTEGER,
            m_ INTEGER,
            samples INTEGER,
            sampling_rate INTEGER,
            measurement_type TEXT
        );
        """)
    except sqlite3.OperationalError as e:
        raise e
        print(e)

def create_table_map_label(db_name=_db):
    """
        Stworzenie tablicy map_info
        Args:
            db_name= lokalizacjia pliku db
    """
    try:
        _connection = get_connection_to_db(db_name)
        _connection.cursor().execute("""
        CREATE TABLE map_label(
            label_id INTEGER PRIMARY KEY AUTOINCREMENT,
            info_id INTEGER NOT NULL,
            channel TEXT,
            data_type TEXT,

            FOREIGN KEY(info_id) REFERENCES map_info(info_id)
        );
        """)
    except sqlite3.OperationalError as e:
        print(e)

def create_table_map_data(db_name=_db):
    """
        Stworzenie tableli map_data
        Args:
            db_name= lokalizacjia pliku db
    """
    try:
        _connection = get_connection_to_db(db_name)
        _connection.cursor().execute("""
            CREATE TABLE map_data(
                data_id INTEGER PRIMARY KEY AUTOINCREMENT,
                label_id integer NOT NULL,
                i integer NOT NULL,
                j integer NOT NULL,
                data_val FLOAT,

                FOREIGN KEY(label_id) REFERENCES map_label(label_id)
        );
        """)
    except sqlite3.OperationalError as e:
        print(e)

def enable_foreign_keys(db_name="test2.db"):
    """
        Uruchomienie kluczy zewnętrznych
    """
    try:
        _connection = get_connection_to_db(db_name)
        _connection.execute("""
        PRAGMA foreign_keys = ON;
        """)
    except sqlite3.OperationalError as e:
        print(e)

def init_db(name='test.db'):
    """
        inicjalizacja bazy danych
        Args:
            name= lokalizacjia pliku db
    """
    get_connection_to_db(name)
    create_table_map_info(name)
    enable_foreign_keys(name)
    create_table_map_label(name)
    create_table_map_data(name)

def select_map_info_filename(file_name,db_name=_db):
    """
        Znajdowanie rekrdów w map_info na zadanej nazwie plików
        Args:
            file_name = nazwa pliku
            db_name= lokalizacjia pliku db
    """
    _connection = get_connection_to_db(db_name)
    _cursor =_connection.cursor()
    _cursor.execute("""
        SELECT * FROM 'map_info' WHERE filename like '"""+file_name+"""%' order by data_zam desc;
    """)
    return _cursor.fetchall()
    #_connection.close()

def check_if_filename_exist(file_name,db_name=_db):
    """
        Sprawdzenie czy dana nazwa instieje w map_info
        Args:
            file_name - nazwa pliku
            db_name= lokalizacjia pliku db
    """
    return select_map_info_filename(file_name,db_name).__len__()>0

def insert_grid_into_map_data(grid,info_id):
    """
        Wprowadzenie danych z mapy do bazy danych
        Args:
            grid - tablica dwuwymiarowa
            db_name= lokalizacjia pliku db
    """
    try:
        _cursor =_connection.cursor()
        for j in grid.shape[1]:
            for i in grid.shape[0]:
                insert_map_data(i,j,grid[i][j],info_id)
    except Exception as e:
        print(e)

def insert_data(filename, x_range ,y_range ,x_middle ,y_middle ,x_div ,y_div ,m_ ,samples ,sampling_rate ,measurement_type,amps,phases,db_name=_db):
    """
        Wprowadzenie całościowe danych do bazy danych
        Args:
            filename - nazwa plik
            x_range - odległość od środka w osi x
            x_range - odległość od środka w osi y
            points - liczba punktów zbieranych z sygnału
            db_name= lokalizacjia pliku db
    """
    q1 = """INSERT INTO map_info(filename, x_range ,y_range ,x_middle ,y_middle ,x_div ,y_div ,m_ ,samples ,sampling_rate ,measurement_type) VALUES ('"""+filename+"""',"""+str(x_range)+""", """+str(y_range)+""", """+str(x_middle)+""", """+str(y_middle)+""","""+str(x_div)+""","""+str(y_div)+""","""+str(m_)+""","""+str(samples)+""","""+str(sampling_rate)+""",'"""+str(measurement_type)+"""');"""
    _connection = get_connection_to_db(db_name)
    _cursor =_connection.cursor()
    _cursor.execute(q1)
    _cursor.execute("""SELECT * FROM 'map_info' WHERE filename='"""+filename+"""';""")
    info_id = _cursor.fetchall()[0][0]
    labels_id_amp = []
    labels_id_ph = []

    for i in range(0,amps.__len__()):
        q2 = """INSERT INTO map_label(info_id, channel,data_type) VALUES ("""+str(info_id)+""",'"""+str("AIN"+str(15-i))+""""','AMP');"""
        _cursor.execute(q2)
        _cursor.execute("""SELECT last_insert_rowid() FROM 'map_label' ;""")
        labels_id_amp.append(_cursor.fetchall()[0][0])
        
    for i in range(0,phases.__len__()):
        q2 = """INSERT INTO map_label(info_id, channel,data_type) VALUES ("""+str(info_id)+""",'"""+str("AIN"+str(15-i))+""""','PHASE');"""
        _cursor.execute(q2)
        _cursor.execute("""SELECT last_insert_rowid() FROM 'map_label' ;""")
        labels_id_ph.append(_cursor.fetchall()[0][0])

    for k in range(0,amps.__len__()):
        grid = amps[k]
        for j in range(0,grid.shape[1]):
            for i in range(0,grid.shape[0]):
                q2 = """INSERT INTO map_data(label_id, i,j,data_val) VALUES ("""+str(labels_id_amp[k])+""","""+str(i)+""","""+str(j)+""","""+str(grid[i][j])+""");"""      
                _cursor.execute(q2)

    for k in range(0,phases.__len__()):
        grid = phases[k]
        for j in range(0,grid.shape[1]):
            for i in range(0,grid.shape[0]):
                q2 = """INSERT INTO map_data(label_id, i,j,data_val) VALUES ("""+str(labels_id_ph[k])+""","""+str(i)+""","""+str(j)+""","""+str(grid[i][j])+""");"""      
                _cursor.execute(q2)

    print("AMP",labels_id_amp)
    print("phase",labels_id_ph)
    _connection.commit()
    _connection.close()


def get_data(id,db_name=_db):
    amps=[]
    phases=[]
    labels=[]
    _connection = get_connection_to_db(db_name)
    q1= """SELECT * FROM 'map_info' WHERE info_id = """+str(id)
    q2= """SELECT * FROM 'map_label' WHERE info_id = """+str(id)
    _cursor =_connection.cursor()
    _cursor.execute(q1)
    info=_cursor.fetchall()[0]
    _cursor.execute(q2)
    print(info)
    labels.append(_cursor.fetchall())
    for i in labels:
        print("labels=",i)
        print("shape=",(2*info[7],2*info[8]))
        for j in i:
            print("label=",j[0])
            q3= """SELECT * FROM 'map_data' WHERE label_id = """+str(j[0])
            _cursor.execute(q3)
            grid = np.zeros((2*info[8],2*info[7]))
            data = _cursor.fetchall()
            for k in data:
                grid[k[2]][k[3]]=k[4]
            if j[3]=="AMP":
                amps.append(grid)
            else:
                phases.append(grid)

    return (info,labels,amps,phases)
    

def test():
    init_db(name="test2.db")
    
    filename="dane_1"
    x_range=1.0
    y_range=1.0
    x_middle=5.0
    y_middle=5.0
    x_div=5
    y_div=5
    m_=1
    samples=1024
    sampling_rate=40000
    measurement_type="slow"
    grid=np.random.rand(2*x_div,2*y_div)
    insert_data(filename, x_range ,y_range ,x_middle ,y_middle ,x_div ,y_div ,m_ ,samples ,sampling_rate ,measurement_type,amps=(grid,),phases=(grid,),db_name="test2.db")
    amps,info,phases,labels=get_data(1,db_name="test2.db")
    print("hello")
    print(info)
    print(labels)
    plt.imshow(amps[0])
    plt.show()