from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure
import tkinter as tk
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
#---------End of imports
"""
    Moduł realizujący wykres sygnału
"""
def signals_plot_init(root,size,time_const):
    """
        Funkcja inicjalizuje i zwraca obiekty do manipulacji wykresem
        Args:
            root - główny obiekt UI
            size - rozmiar wykresów tj. rozmiar tablicy z próbkami
            time_const - stała czasowa wynikająca z częśtości próbkowania
        Returns:
             fig - obiekt matplotlib
             ax - oś x
             line1 - wykres1
             line2 - wykres2
             canvas - obiekt ui
    """
    fig, ax = plt.subplots(2, sharex=True,figsize=(10,6))
    canvas = FigureCanvasTkAgg(fig,master=root)
    def auto_fun():
        ax[0].relim()
        ax[0].autoscale_view(True,True,True)
        ax[1].relim()
        ax[1].autoscale_view(True,True,True)     
        canvas.draw()
    auto_btn = tk.Button(master=root, text='AUTO', command=auto_fun)    
    auto_btn.grid(column=0,row=1)
    
    canvas.get_tk_widget().grid(column=0,row=0)
    ax[0].grid(True)
    ax[1].grid(True)
    ax[0].set_ylabel('Ref [V]')
    ax[1].set_ylabel('Sig [V]')
    ax[1].set_xlabel('t [ms]')
    line1, = ax[0].plot(np.arange(0,size)*time_const, np.sin(np.arange(0,size)*time_const),'b-')
    line2, = ax[1].plot(np.arange(0,size)*time_const, np.sin(np.arange(0,size)*time_const),'r-')
    return fig,ax,line1,line2,canvas

def test():
    root = tk.Tk()
    plot = SignalPlot(root,0,0,1,"N")
    #ani = animation.FuncAnimation(plot.fig, plot.animate, np.arange(1, 200), interval=25, blit=False)
    tk.mainloop()
