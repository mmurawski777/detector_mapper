"""
    Moduł przechowujący część interfejsu przechowującego parametry pomiaru
"""

from tkinter import *
from datetime import datetime

class MeasurementPanel(object):
    def __init__(self,root,column,row,osc_ofon_fun=None):
        self.column=column
        self.row=row
        self.filename_var = StringVar()
        self.filename_var.set(datetime.now().strftime('%Y-%m-%d')+"_")
        self.x_var = StringVar()
        self.x_var.set("0.5")
        self.y_var = StringVar()
        self.y_var.set("0.5")
        self.x_div = StringVar()
        self.x_div.set("100")
        self.y_div = StringVar()
        self.y_div.set("100")
        self.osc_var = StringVar()
        self.osc_var.set("OSC: ON")
        self.m_var = StringVar()
        self.m_var.set("20")
        self.lbl0 = Label(root,text="MEASUREMENT PARAMETERS")
        self.lbl0.grid(column=column,row=row,sticky="NW")
        self.filename_lbl = Label(root,text="file_name = ")
        self.filename_lbl.grid(column=column,row=row+1,sticky="NW")
        self.filename_entry = Entry(root,textvariable=self.filename_var,width=25)
        self.filename_entry.grid(column=1+column,row=row+1,sticky="NW",columnspan=3)

        self.x_var_lbl = Label(root,text="x_+/-[mm] = ")
        self.x_var_lbl.grid(column=column,row=row+2,sticky="NW")
        self.x_var_entry = Entry(root,textvariable=self.x_var,width=10)
        self.x_var_entry.grid(column=1+column,row=row+2,sticky="NW")

        self.y_var_lbl = Label(root,text="y_+/-[mm] = ")
        self.y_var_lbl.grid(column=2+column,row=row+2,sticky="NW")
        self.y_var_entry = Entry(root,textvariable=self.y_var,width=10)
        self.y_var_entry.grid(column=3+column,row=row+2,sticky="NW")

        self.x_div_lbl = Label(root,text="x_div = ")
        self.x_div_lbl.grid(column=column,row=row+3,sticky="NW")
        self.x_div_entry = Entry(root,textvariable=self.x_div,width=10)
        self.x_div_entry.grid(column=1+column,row=row+3,sticky="NW")

        self.y_div_lbl = Label(root,text="y_div = ")
        self.y_div_lbl.grid(column=2+column,row=row+3,sticky="NW")
        self.y_div_entry = Entry(root,textvariable=self.y_div,width=10)
        self.y_div_entry.grid(column=3+column,row=row+3,sticky="NW")
        
        self.m_entry = Entry(root,textvariable=self.m_var,width=10)
        self.m_entry.grid(column=1+column,row=row+5,sticky="NW")#,columnspan=2)
        self.m_lbl = Label(root,text="M = ")
        self.m_lbl.grid(column=column,row=row+5,sticky="NW")
        self.osc_btn = Button(root, textvariable=self.osc_var, command=osc_ofon_fun)
        self.osc_btn.grid(column=column+3,row=row+6,sticky="ESNW")

    def disable(self):
        """
            Funkcja blokująca UI
        """
        self.filename_entry['state']='disabled'
        self.x_var_entry['state']='disabled'
        self.y_var_entry['state']='disabled'
        self.x_div_entry['state']='disabled'
        self.y_div_entry['state']='disabled'
        self.m_entry['state']='disabled'
        #self.average_entry['state']='disabled'
        #self.points_entry['state']='disabled'
        #self.option_mnu['state']='disabled'
        self.osc_btn['state']='disabled'
        
    def enable(self):
        """
            Funkcja odblokowująca UI
        """
        self.filename_entry['state']='normal'
        self.x_var_entry['state']='normal'
        self.y_var_entry['state']='normal'
        self.x_div_entry['state']='normal'
        self.y_div_entry['state']='normal'
        self.m_entry['state']='normal'
        #self.average_entry['state']='normal'
        #self.points_entry['state']='normal'
        #self.option_mnu['state']='normal'
        self.osc_btn['state']='normal'
    
    @property
    def osc(self):
        return self.osc_var.get()

    @osc.setter
    def osc(self,val):
        if(val):
            self.osc_var.set("OSC: ON")
        else:
            self.osc_var.set("OSC:OFF")
    @property
    def filename(self):
        return self.filename_var.get()
    @filename.setter
    def filename(self,val):
        self.filename_var.set(val)
    @property
    def m(self):
        return float(self.m_var.get())
    @m.setter
    def m(self,val):
        self.m_var.set(str(val))
    @property
    def nx(self):
        return int(self.x_div.get())
    @property
    def ny(self):
        return int(self.y_div.get())
    @nx.setter
    def nx(self,val):
        self.x_div.set(str(val))
    @ny.setter
    def ny(self,val):
        self.y_div.set(str(val))
    @property
    def x_range(self):
        return float(self.x_var.get())
    @x_range.setter
    def x_range(self,val):
        self.x_var.set(str(val))
    @property
    def y_range(self):
        return float(self.y_var.get())
    @y_range.setter
    def y_range(self,val):
        self.y_var.set(str(val))
   
    @property
    def dx(self):
        return float(self.x_var.get())/float(self.x_div.get())
    @property
    def dy(self):
        return float(self.y_var.get())/float(self.y_div.get())


def test():
    """
        Funkcja testowa
    """
    def fun():
        pass
    
    root = Tk()
    messurement_panel = MessurementPanel(root,0,0,fun)
    messurement_panel.osc=False
    print(messurement_panel.filename)
    #print(messurement_panel.points)
    print(messurement_panel.dx)
    print(messurement_panel.dy)
    messurement_panel.disable()
    messurement_panel.enable()
    root.mainloop()

if __name__=="__main__":
    test()