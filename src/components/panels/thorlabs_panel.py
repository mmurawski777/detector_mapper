from tkinter import *

class ThorlabsPanel(object):
    def __init__(self,root,column,row,init_apt_fun=None,find_middle_fun=None,go_to_middle_fun=None,
    x_move_plus_fun=None,x_move_minus_fun=None,y_move_plus_fun=None,y_move_minus_fun=None,
    set_middle_fun=None,stop_motor=None,move_to_x=None,move_to_y=None
    ):
        self.root=root
        self.column=column
        self.row=row
        self.x_pos=StringVar()
        self.x_pos.set("0.00")
        self.y_pos=StringVar()
        self.y_pos.set("0.00")
        self.x0_pos=StringVar()
        self.x0_pos.set("x0: 0.00")
        self.y0_pos=StringVar()
        self.y0_pos.set("y0: 0.00")
        self.label1 = Label(self.root,text="POSITIONER CALIBRATION").grid(
            column=column,row=row,sticky="W")
        self.init_apt_btn = Button(self.root, text='INIT APT', command=init_apt_fun)
        self.init_apt_btn.grid(column=column,row=row+1,sticky="WE")
        self.go_to_middle_btn = Button(self.root, text='GO TO MIDDLE', command=go_to_middle_fun)
        self.go_to_middle_btn.grid(column=column+1,row=row+1,sticky="WE")
        self.find_middle_btn = Button(self.root, text='FIND MIDDLE', command=find_middle_fun)
        self.find_middle_btn.grid(column=column+2,row=row+1,sticky="WE")
        self.set_middle_btn = Button(self.root, text='SET MIDDLE', command=set_middle_fun)
        self.set_middle_btn.grid(column=column+3,row=row+1,sticky="WE")

        self.x_move_minus_btn = Button(self.root, text='<<', command=None)
        self.x_move_minus_btn.grid(column=column+2,row=row+2,sticky="E")
        self.x_move_minus_btn.bind('<Button-1>',x_move_minus_fun)
        self.x_move_plus_btn = Button(self.root, text='>>', command=None)
        self.x_move_plus_btn.grid(column=column+3,row=row+2,sticky="W")
        self.x_move_plus_btn.bind('<Button-1>',x_move_plus_fun)
        self.y_move_minus_btn = Button(self.root, text='<<', command=None)
        self.y_move_minus_btn.grid(column=column+2,row=row+3,sticky="E")
        self.y_move_minus_btn.bind('<Button-1>',y_move_minus_fun)
        self.y_move_plus_btn = Button(self.root, text='>>', command=None)
        self.y_move_plus_btn.grid(column=column+3,row=row+3,sticky="W")
        self.y_move_plus_btn.bind('<Button-1>',y_move_plus_fun)
        
        self.y_move_plus_btn.bind('<ButtonRelease-1>',stop_motor)
        self.y_move_minus_btn.bind('<ButtonRelease-1>',stop_motor)
        self.x_move_plus_btn.bind('<ButtonRelease-1>',stop_motor)
        self.x_move_minus_btn.bind('<ButtonRelease-1>',stop_motor)

        self.x0_label =  Label(self.root,textvariable=self.x0_pos).grid(
            column=column,row=row+2,sticky="W")
        self.y0_label =  Label(self.root,textvariable=self.y0_pos).grid(
            column=column,row=row+3,sticky="W")
        self.x_entry =  Entry(self.root,textvariable=self.x_pos)
        self.x_entry.grid(column=column+1,row=row+2,sticky="W",columnspan=2)
        
        
        self.y_entry =  Entry(self.root,textvariable=self.y_pos)
        self.y_entry.grid(
            column=column+1,row=row+3,sticky="W",columnspan=2)
        
        self.x_entry.bind('<Double-Button-1>', self.enable_x)
        self.y_entry.bind('<Double-Button-1>', self.enable_y)
        self.x_entry.bind("<Return>", self.go_to_x)
        self.y_entry.bind("<Return>", self.go_to_y)
        self.x_entry['state']='disabled'
        self.y_entry['state']='disabled'
        self.move_to_x=move_to_x
        self.move_to_y=move_to_y


    def go_to_x(self,event):
        try:
            self.move_to_x()
        except Exception as e:
            print(e)
        self.x_entry['state']='disabled'

    def go_to_y(self,event):
        try:
            self.move_to_y()
        except Exception as e:
            print(e)
        self.y_entry['state']='disabled'
    
    def enable_x(self,event):
            self.x_entry['state']='normal'
    def enable_y(self,event):
            self.y_entry['state']='normal'

    def disable(self):
        self.init_apt_btn['state']='disabled'
        self.go_to_middle_btn['state']='disabled'
        self.find_middle_btn['state']='disabled'
        self.x_move_minus_btn['state']='disabled'
        self.x_move_plus_btn['state']='disabled'
        self.y_move_minus_btn['state']='disabled'
        self.y_move_plus_btn['state']='disabled'
        self.set_middle_btn['state']='disabled'

    def enable(self):
        self.init_apt_btn['state']='active'
        self.go_to_middle_btn['state']='active'
        self.find_middle_btn['state']='active'
        self.x_move_minus_btn['state']='active'
        self.x_move_plus_btn['state']='active'
        self.y_move_minus_btn['state']='active'
        self.y_move_plus_btn['state']='active'
        self.set_middle_btn['state']='active'
    @property
    def x(self):
        return float(self.x_pos.get())
    @property
    def xy(self):
        return float(self.x_pos.get()),float(self.y_pos.get())
    @property
    def xy0(self):
        return float(self.x0_pos.get()[3:]),float(self.y0_pos.get()[3:])
    @x.setter
    def x(self,x):
        self.x_pos.set(str(x))
    @xy.setter
    def xy(self,x):
        self.x_pos.set(str(x[0]))
        self.y_pos.set(str(x[1]))
    @xy0.setter
    def xy0(self,x):
        self.x0_pos.set("x0: "+str(x[0]))
        self.y0_pos.set("x0: "+str(x[1]))
    @property
    def y(self):
        return float(self.y_pos.get())
    @y.setter
    def y(self,x):
        self.y_pos.set(str(x))
    @property
    def x0(self):
        return float(self.x0_pos.get()[3:])
    @x0.setter
    def x0(self,x):
        self.x0_pos.set("x0: "+str((x)))
    @property
    def y0(self):
        return float(self.y0_pos.get()[3:])
    @y0.setter
    def y0(self,x):
        self.y0_pos.set("y0: "+str(x))
    
        
            
def test():
    root = Tk()
    thorlabs_panel=ThorlabsPanel(root,0,0)
    thorlabs_panel.y=1.000
    thorlabs_panel.x=2.000
    thorlabs_panel.y0=3.000
    thorlabs_panel.x0=4.000
    thorlabs_panel.disable()
    root.mainloop()