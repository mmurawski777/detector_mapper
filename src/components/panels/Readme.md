# Panels
* dmap.py
    * klasa realizująca wykres mapy detektora

            class DMap:
                def __init__(self,root,column,row,rowspan=1,columnspan=1,scale="log",vmin=0.001,vmax=10.0):

* menu_bar.py
    * klasa realizująca pasek menu

            class MenuBar(object):
                def __init__(self,root,file_save_as_csv_amp=None,file_save_as_png_amp=None,file_save_as_csv_phase=None,file_save_as_png_phase=None,open_from_db=None,hello=None):
                    
* measurement_panel.py
    * W klasie zostały użyte dekoratory w razie nie jasności proszę przeczytać https://www.python-course.eu/python3_properties.php
        
            class MeasurementPanel(object):
                def __init__(self,root,column,row,osc_ofon_fun=None):

* thorlabs_panel.py
    * panel przechowuje informacje na temat parametrów manipulatorów. Konstruktor przyjmuje funkcje które obsługują sterowanie manipulatorów

                class ThorlabsPanel(object):
                    def __init__(self,root,column,row,init_apt_fun=None,find_middle_fun=None,go_to_middle_fun=None,
                    x_move_plus_fun=None,x_move_minus_fun=None,y_move_plus_fun=None,y_move_minus_fun=None,
                    set_middle_fun=None,stop_motor=None,move_to_x=None,move_to_y=None)
* settings_panel.py
    * Klasa dziedziczy po obiektach MeasurementPanel i ThorlabsPanel
            
            class SettingsPanel(MeasurementPanel,ThorlabsPanel)

* singals_plot.py
    * Plik przechowuje jedynie funkcje która zwraca elementu wykresu. Jest to w inny sposób zrealizowane ponieważ wykres może być modyfikowany jedynie bezpośrednio w wątku głównym bez odwoływania się do metod większej klasy

            def signals_plot_init(root,size,time_const):
             """
                Funkcja inicjalizuje i zwraca obiekty do        manipulacji wykresem
                Args:
                    root - główny obiekt UI
                    size - rozmiar wykresów tj. rozmiar         tablicy z próbkami
                    time_const - stała czasowa      wynikająca z częśtości próbkowania
                Returns:
                     fig - obiekt matplotlib
                     ax - oś x
                     line1 - wykres1
                     line2 - wykres2
                     canvas - obiekt ui
            """
                ...
                return fig,ax,line1,line2,canvas
            