"""@package docstring
Moduł przechowujący SettingPanel jest to obiekt który dziedziczy z MeasurementPanel i ThorlabsPanel
"""

from .measurement_panel import MeasurementPanel
from .thorlabs_panel import ThorlabsPanel
from tkinter import *

class SettingsPanel(MeasurementPanel,ThorlabsPanel):
    def __init__(self,root,column,row,osc_ofon_fun=None,init_apt_fun=None,
    find_middle_fun=None,go_to_middle_fun=None,x_move_plus_fun=None,
    x_move_minus_fun=None,y_move_plus_fun=None,y_move_minus_fun=None,
    set_middle_fun=None,start_btn_fun=None,pause_btn_fun=None,stop_motor=None
    ,move_to_x=None,move_to_y=None):
        """
            Kontruktor dziedzizący
            Args:
                column kolumna
                row  rząd
                osc_onfon_fun - funkcja do włączania i wyłączania oscyloskopu
                init_apt_fun - funkcja do inicjalizacji protokołu apt
                find_middle_fun - funkcja do znajdowania środka
                go_to_middle_fun - funkcja do przesuwania manipulatorów do środka
                x_move_plus_fun - funkcja do poruszania manipulatorem w kierunku dodatniam w osi x
                x_move_minus_fun - funkcja do poruszania manipulatorem w kierunku ujemnym w osi x
                y_move_plus_fun - funkcja do poruszania manipulatorem w kierunku dodatniam w osi y
                y_move_minus_fun - funkcja do poruszania manipulatorem w kierunku ujemnym w osi y
                start_btn_fun - funkcja do uruchomienia pomiaru
                pause_btn_fun - funkcja do zatrzymania pomiaru
                stop_motor - funkcja do zatrzymania silników
            
        """
        MeasurementPanel.__init__(self,root=root,column=column,row=row,osc_ofon_fun=osc_ofon_fun)
        ThorlabsPanel.__init__(self,root=root,column=column,row=row+7,init_apt_fun=init_apt_fun,
    find_middle_fun=find_middle_fun,go_to_middle_fun=go_to_middle_fun,x_move_plus_fun=x_move_plus_fun,
    x_move_minus_fun=x_move_minus_fun,y_move_plus_fun=y_move_plus_fun,y_move_minus_fun=y_move_minus_fun,
    set_middle_fun=set_middle_fun,stop_motor=stop_motor,move_to_x=move_to_x,move_to_y=move_to_y)
    

        self.start_btn = Button(self.root,text="START", command=start_btn_fun)
        self.start_btn.grid(column=self.column,row=self.row+8,sticky="WE")
        self.stop_btn = Button(self.root,text="PAUSE", command=pause_btn_fun)
        self.stop_btn.grid(column=self.column+1,row=self.row+8,sticky="WE")
        self.stop_btn['state']='disable'
        #self.ThorlabsPanel.x_label['state']='disabled'
        #self.y_label['state']='disabled'
    def disable(self):
        MeasurementPanel.disable(self)
        ThorlabsPanel.disable(self)
    def disable_apt(self):
        ThorlabsPanel.disable(self)
    def enable_apt(self):
        ThorlabsPanel.enable(self)
    def enable(self):
        MeasurementPanel.enable(self)
        ThorlabsPanel.enable(self)

def test():
    root = Tk()
    def fun():
        pass
    panel = SettingsPanel(root,0,0)
    panel.disable()
    mainloop()
