import tkinter as tk
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure
import matplotlib as mpl
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib.ticker import FuncFormatter, MaxNLocator

class DMap:
    def __init__(self,root,column,row,rowspan=1,columnspan=1,scale="log",vmin=0.001,vmax=10.0):
        self.scale=scale
        self.root=root
        self.column=column
        self.vmax=vmax
        self.vmin=vmin
        self.rowspan=rowspan
        self.row=row
        self.fig, self.ax = plt.subplots(figsize=(10,6))
        self.canvas=FigureCanvasTkAgg(self.fig,master=self.root)
        self.canvas.get_tk_widget().grid(column=self.column,row=self.row,columnspan=columnspan,rowspan=rowspan)
        self.ax.yaxis.set_visible(True)
        self.ax.xaxis.set_visible(True)
        self.ax.grid(True)
        self.ax.axes.set_frame_on(True)
        self.slider = tk.Scale(self.root, from_=vmin, to=vmax,length=350,
            orient='horizontal',command=self.change_value,digits=5,resolution=vmin)
        self.slider2 = tk.Scale(self.root, from_=vmin, to=vmax,length=350,
            orient='horizontal',command=self.change_value2,digits=5,resolution=vmin)
        frame=np.zeros((10,10))

        if(self.scale=="log"):
            self.im=self.ax.imshow(frame, origin = 'lower',cmap=mpl.cm.jet,
            vmin=vmin, vmax=vmax,norm=mpl.colors.LogNorm(vmin=vmin, vmax=vmax))
            self.cbar = self.fig.colorbar(self.im)
            self.cbar.set_clim(vmin=vmin, vmax=vmax)
            self.canvas.draw()
            self.slider.grid(column=self.column,row=self.row+rowspan,rowspan=self.rowspan)
            self.slider.set((vmax-vmin)/2)

            self.slider2.grid(column=self.column,row=self.row+rowspan+1,rowspan=self.rowspan)
            self.slider2.set((vmax-vmin)/2)
        elif(self.scale=="lin"):
            self.im=self.ax.imshow(frame, origin = 'lower',cmap=mpl.cm.jet,
            vmin=vmin, vmax=vmax)
            self.cbar = self.fig.colorbar(self.im)
            self.cbar.set_clim(vmin=vmin, vmax=vmax)
            self.canvas.draw()
            self.slider = tk.Scale(self.root, from_=vmin, to=vmax,length=350,
            orient='horizontal',command=self.change_value,digits=5,resolution=vmin)
            self.slider.grid(column=self.column,row=self.row+rowspan,rowspan=self.rowspan)
            self.slider.set((vmax-vmin)/2)

            self.slider2 = tk.Scale(self.root, from_=vmin, to=vmax,length=350,
            orient='horizontal',command=self.change_value2,digits=5,resolution=vmin)
            self.slider2.grid(column=self.column,row=self.row+rowspan+1,rowspan=self.rowspan)
            self.slider2.set(vmin)


    def save_img(self,name):
        self.im.savefig(name)
    def set_grid(self,grid):
        self.im.set_data(grid)
        #self.ax.relim()
        self.canvas.draw()

    def reshape(self,ix,iy,dx,dy,_x,_y):
        if(self.scale=="log"):
            self.im=self.ax.imshow(np.zeros((ix,iy)), origin = 'lower',cmap=mpl.cm.jet,vmin=self.vmin,
            vmax=self.vmax,norm=mpl.colors.LogNorm(vmin=self.vmin, vmax=self.vmax))
        else:
            self.im=self.ax.imshow(np.zeros((ix,iy)), origin = 'lower',cmap=mpl.cm.jet,vmin=self.vmin,
            vmax=self.vmax,norm=mpl.colors.Normalize(vmin=self.vmin, vmax=self.vmax))

        self.ax.xaxis.set_major_formatter(FuncFormatter(lambda tick_val,tick_pos: round(int(tick_val)*dx-_x,2)))
        self.ax.xaxis.set_major_locator(MaxNLocator(integer=True))
        self.ax.yaxis.set_major_formatter(FuncFormatter(lambda tick_val,tick_pos: round(int(tick_val)*dy-_y,2)))
        self.ax.yaxis.set_major_locator(MaxNLocator(integer=True))

    def get_array(self):
        return self.im.get_array()
    def change_value2(self,*args):
        if(self.scale=="log"):
            if(self.slider2.get()<self.slider.get()):
                self.im.norm=mpl.colors.LogNorm(vmin=self.slider2.get(), vmax=self.slider.get())
            else:
                self.slider2.set(self.vmin)
        else:
            if(self.slider2.get()<self.slider.get()):
                self.im.norm=mpl.colors.Normalize(vmin=self.slider2.get(), vmax=self.slider.get())
            else:
                self.slider2.set(self.vmin)
        
        self.cbar.remove()
        self.cbar = self.fig.colorbar(self.im)
        self.cbar.set_clim(vmin=self.slider2.get(), vmax=self.slider.get())
        self.canvas.draw()
    def change_value(self,*args):
        if(self.scale=="log"):
            if(self.slider.get()>self.slider2.get()):
                self.im.norm=mpl.colors.LogNorm(vmin=self.slider2.get(), vmax=self.slider.get())
            else:
                self.slider.set(self.vmax)
        else:
            if(self.slider.get()>self.slider2.get()):
                self.im.norm=mpl.colors.Normalize(vmin=self.slider2.get(), vmax=self.slider.get())
            else:
                self.slider.set(self.vmax)
        
        self.cbar.remove()
        self.cbar = self.fig.colorbar(self.im)
        self.cbar.set_clim(vmin=self.slider2.get(), vmax=self.slider.get())
        self.canvas.draw()
    def redraw(self):
        self.canvas.draw()

def test():
    root=tk.Tk()
    def close():
        root.quit()     
        root.destroy()
    grid =np.random.rand(10,10) 
    dmap=DMap(root,0,0)
    dmap.set_grid(grid)
    root.protocol("WM_DELETE_WINDOW",close)
    root.mainloop()