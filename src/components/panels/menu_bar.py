"""
    Moduł realizujący gówny panel menu na opcje zapisu wyników w formacie .csv , png
"""
from tkinter import *

class MenuBar(object):
        def __init__(self,root,file_save_as_csv_amp=None,file_save_as_png_amp=None,
            file_save_as_csv_phase=None,file_save_as_png_phase=None,open_from_db=None,hello=None):
            """
                Konstruktor, obiekt jest wciaz rozbudowywany wiec w niektórych miejscach jest funkcja robiaca nic czyli
                hello
                Args:
                    root - tkinter root
                    file_save_as_csv_amp - funkcja zapisu amplitudy w csv
                    file_save_as_png_amp    - funkcja zapisu amplitudy w png
                    file_save_as_csv_phase - funkcja zapisu fazy w csv
                    file_save_as_png_phase  - funkcja zapisu fazy png
                    hello - funkcja testowa 
            """
            self.go_to_max = BooleanVar()
            self.line_measurement= BooleanVar()
            menubar = Menu(root)
            filemenu = Menu(menubar, tearoff=0)
            filemenu.add_command(label="Open from Data Base", command=open_from_db)
            filemenu.add_separator()
            filemenu.add_command(label="Export amplitude to .csv", command=file_save_as_csv_amp)
            filemenu.add_command(label="Export amplitude to .png", command=file_save_as_png_amp)
            filemenu.add_command(label="Export phase to .csv", command=file_save_as_csv_phase)
            filemenu.add_command(label="Export phase to .png", command=file_save_as_png_phase)
            menubar.add_cascade(label="File", menu=filemenu)

            editmenu = Menu(menubar, tearoff=0)
            editmenu.add_checkbutton(label="go to maximum after measurement",
             onvalue=1, offvalue=False, variable=self.go_to_max)
            menubar.add_cascade(label="Options", menu=editmenu)
            helpmenu = Menu(menubar, tearoff=0)
            helpmenu.add_command(label="Report Errors", command=hello)
            helpmenu.add_command(label="Read Documentation", command=hello)
            menubar.add_cascade(label="Help", menu=helpmenu)

            root.config(menu=menubar)

