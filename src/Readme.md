# Src
Folder src przechowuje składowe elementy programu
 * apt
    * biblioteka do komunikacji z protokołem APT
    * w folderze musi znajdować się plik o nazwie APT.dll, żeby biblioteka odpowiedzialna za komunikację z protokołem APT zadziałała
 * components
    * elemnty interfejsu graficznego
 * daq
    * zwarappowany interfejs do osługi karty pomiarowej w python ctypes
    * folder musi zawierać skompilowany plik Usb4716.cpp do pliku Usb4716.dll, żeby komunikacja z kartą akwizycji zadziałała
 * db
    * funkcje do obsługi lokalnej bazy danych
    * Folder musi zawierać plik test.db z utworzonymi tabelami danych, żeby zapis do bazy danych zadziałał. Jesli w folderze nie znajduje się plik z bazą danych .db
    -należy ją utorzyć wywołując funkcję z pliku sqlite_functions.py

            def init_db(name)
        w celu uwtorzenia bazy danych
 * test
    * testy komponentów
 * settings.cfg
    * plik przechowujący ustawienia np. lokalizacj
 * ustils.py
    * plik przechowujący metody do zapisywania i odczytywania ustawień z pliku settings.cfg
